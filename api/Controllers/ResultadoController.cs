using System;
using api.enums;
using api.services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace api.controllers
{
    public class ResultadoController : Controller
    {
        #region Get
        [HttpGet]
        [Authorize]
        [Route("api/get/resultadoscategoria")]
        public IActionResult GetResultadosCategoria(DateTime dataInicial, DateTime dataFinal)
        {
            try
            {
                var resultadosCategoria = ResultadoService.GetAllResultadoCategoria(dataInicial, dataFinal);
                var response = ResultadoService.ValidationResponse;
                if (response.Type.Equals("Error"))
                {
                    return StatusCode((int) StatusCodeEnum.Error, response.Message);
                }
                else
                {
                    return StatusCode((int) StatusCodeEnum.Success, new
                    {
                        Response = response,
                        ResultadoCategoria = resultadosCategoria
                    });
                }
            }
            catch
            {
                return BadRequest("Erro ao recuperar resultados de categoria.");
            }
        }

        [HttpGet]
        [Authorize]
        [Route("api/get/resultadosdesempenho")]
        public IActionResult GetResultadosDesempenho(int projetoId)
        {
            try
            {
                var resultadosDesempenho = ResultadoService.GetAllResultadoDesempenho(projetoId);
                var response = ResultadoService.ValidationResponse;
                if (response.Type.Equals("Error"))
                {
                    return StatusCode((int) StatusCodeEnum.Error, response.Message);
                }
                else
                {
                    return StatusCode((int) StatusCodeEnum.Success, new
                    {
                        Response = response,
                        ResultadosDesempenho = resultadosDesempenho
                    });
                }
            }
            catch
            {
                return BadRequest("Erro ao recuperar resultados de desempenho.");
            }
        }

        [HttpGet]
        [Authorize]
        [Route("api/get/resultadoprojeto")]
        public IActionResult GetResultadoProjeto(int projetoId)
        {
            try
            {
                var resultadoProjeto = ResultadoService.GetAllResultadoProjeto(projetoId);
                var response = ResultadoService.ValidationResponse;
                if (response.Type.Equals("Error"))
                {
                    return StatusCode((int) StatusCodeEnum.Error, response.Message);
                }
                else
                {
                    return StatusCode((int) StatusCodeEnum.Success, new
                    {
                        Response = response,
                        ResultadoProjeto = resultadoProjeto
                    });
                }
            }
            catch
            {
                return BadRequest("Erro ao recuperar resultados de projeto.");
            }
        }

        [HttpGet]
        [Authorize]
        [Route("api/get/resultadotarefas")]
        public IActionResult GetResultadoTarefas(int projetoId)
        {
            try
            {
                var resultadoTarefas = ResultadoService.GetAllResultadoTarefas(projetoId);
                var response = ResultadoService.ValidationResponse;
                if (response.Type.Equals("Error"))
                {
                    return StatusCode((int) StatusCodeEnum.Error, response.Message);
                }
                else
                {
                    return StatusCode((int) StatusCodeEnum.Success, new
                    {
                        Response = response,
                        ResultadoTarefas = resultadoTarefas
                    });
                }
            }
            catch
            {
                return BadRequest("Erro ao recuperar resultados de tarefas.");
            }
        }
        #endregion
    }
}