using System;
using System.Linq;
using api.enums;
using api.models;
using api.services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace api.controllers
{
    public class ProjetoController : Controller
    {

        #region Post
        [HttpPost]
        [Route("api/add/projeto")]
        [Authorize]
        public IActionResult Post([FromBody] Projeto projeto)
        {
            try
            {
                ProjetoService.Add(projeto);
                var response = ProjetoService.ValidationResponse;
                if (response.Type.Equals("Error"))
                {
                    return StatusCode((int)StatusCodeEnum.Error, response.Message);
                }
                else
                {
                    return StatusCode((int)StatusCodeEnum.Success, new
                    {
                        Response = response
                    });
                }
            }
            catch
            {
                return BadRequest("Erro ao cadastrar projeto.");
            }
        }
        #endregion
        #region Get
        [HttpGet]
        [Authorize]
        [Route("api/get/projeto")]
        public IActionResult Get()
        {
            try
            {
                var projetos = ProjetoService.GetAll();
                var response = ProjetoService.ValidationResponse;
                if (response.Type.ToString().Equals("Error"))
                {
                    return StatusCode((int) StatusCodeEnum.Error, response.Message);
                }
                else
                {
                    return StatusCode((int) StatusCodeEnum.Success, new
                    {
                        Response = response,
                            Projetos = projetos
                    });
                }
            }
            catch
            {
                return BadRequest("Erro ao recuperar projetos.");
            }
        }
        #endregion
        #region DELETE
        [Route("api/delete/projeto")]
        [HttpDelete]
        [Authorize]
        public IActionResult Delete(int projetoId) 
        {
            ProjetoService.Delete(projetoId);
            var response = ProjetoService.ValidationResponse;
            if (response.Type.Equals("Error"))
            {
                return StatusCode((int) StatusCodeEnum.Error, response.Message);
            }
            else
            {
                return StatusCode((int) StatusCodeEnum.Success, new
                {
                    Response = response,
                });
            }
        }
        #endregion
    }
}