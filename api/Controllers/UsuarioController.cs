using System;
using System.Linq;
using System.Security.Claims;
using api.enums;
using api.models;
using api.services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace api.controllers
{
    public class UsuarioController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly TokenService _tokenService;

        public UsuarioController(IConfiguration configuration)
        {
            _configuration = configuration;
            _tokenService = new TokenService(configuration);
        }
        
        #region Post
        [HttpPost]
        [Route("api/add/usuario")]
        [Authorize]
        public IActionResult Post([FromBody] Usuario usuario)
        {
            try
            {
                UsuarioService.Add(usuario);
                var response = UsuarioService.ValidationResponse;
                if (response.Type.ToString().Equals("Error"))
                {
                    return StatusCode((int) StatusCodeEnum.Error, response.Message);
                }
                else
                {
                    return StatusCode((int) StatusCodeEnum.Success, new
                    {
                        Response = response
                    });
                }
            }
            catch
            {
                return BadRequest("Erro ao cadastrar usuario.");
            }
        }
        #endregion
        #region Get
        [HttpGet]
        [Route("api/getAll/usuario")]
        [Authorize]
        public IActionResult GetAll()
        {
            try
            {
                var usuarios = UsuarioService.GetAll();
                var response = UsuarioService.ValidationResponse;
                if (response.Type.Equals("Error"))
                {
                    return StatusCode((int) StatusCodeEnum.Error, response.Message);
                }
                else
                {
                    return StatusCode((int) StatusCodeEnum.Success, new
                    {
                        Response = response,
                        Usuarios = usuarios
                    });
                }
            }
            catch
            {
                return BadRequest("Erro ao recuperar usuários.");
            }
        }
        #endregion
        #region DELETE
        [Route("api/delete/usuario")]
        [HttpDelete]
        [Authorize]
        public IActionResult Delete(int usuarioId) 
        {
            UsuarioService.Delete(usuarioId);
            var response = UsuarioService.ValidationResponse;
            if (response.Type.Equals("Error"))
            {
                return StatusCode((int) StatusCodeEnum.Error, response.Message);
            }
            else
            {
                return StatusCode((int) StatusCodeEnum.Success, new
                {
                    Response = response,
                });
            }
        }
        #endregion
        #region PUT
        [HttpPut]
        [Authorize]
        [Route("api/put/trocaSenha")]
        public IActionResult TrocarSenha(string senhaAntiga, string senhaNova)
        {
            try
            {
                var usuarioId = _tokenService.GetIdToken(User);
                UsuarioService.AlterarSenha(usuarioId, senhaAntiga, senhaNova);
                var response = UsuarioService.ValidationResponse;
                if (response.Type.ToString().Equals("Error"))
                {
                    return StatusCode((int) StatusCodeEnum.Error, response.Message);
                }
                else
                {
                    return StatusCode((int) StatusCodeEnum.Success, new
                    {
                        Response = response
                    });
                }
            }
            catch
            {
                return BadRequest("Erro ao trocar senha.");
            }
        }
        #endregion
    }
}