using api.enums;
using api.models;
using api.services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace api.controllers
{
    public class AccountController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly TokenService _tokenService;

        public AccountController(IConfiguration configuration)
        {
            _configuration = configuration;
            _tokenService = new TokenService(configuration);
        }

        #region TOKEN
        [AllowAnonymous]
        [HttpPost("token")]
        [Route("api/account/token")]
        public IActionResult CreateToken([FromBody] Login login)
        {
            try
            {
                var usuario = UsuarioService.Get(login.Username, login.Password);
                var response = UsuarioService.ValidationResponse;
                if (response.Type.ToString().Equals("Error"))
                {
                    return StatusCode((int) StatusCodeEnum.Error, response.Message);
                }

                var expiresIn = 60;

                var token = _tokenService.GenerateSignToken(usuario);

                return Ok(new
                {
                    AccessToken = token,
                        TokenType = "Bearer",
                        ExpiresIn = expiresIn
                });
            }
            catch
            {
                return BadRequest("Erro ao obter token.");
            }
        }
        #endregion
        #region GET
        [HttpGet]
        [Authorize]
        [Route("api/get/userAutentication")]
        public IActionResult Get()
        {
            try
            {
                var usuarioId = _tokenService.GetIdToken(User);
                var usuario = UsuarioService.ObtemPorId(usuarioId);
                var response = UsuarioService.ValidationResponse;
                if (response.Type.ToString().Equals("Error"))
                {
                    return StatusCode((int) StatusCodeEnum.Error, response.Message);
                }
                else
                {
                    return StatusCode((int) StatusCodeEnum.Success, new
                    {
                        Response = response,
                            Usuario = usuario
                    });
                }
            }
            catch
            {
                return BadRequest("Erro ao recuperar usuário.");
            }
        }

        [HttpGet]
        [Authorize]
        [Route("api/get/verificaAutenticacao")]
        public string VerificaAutenticacao()
        {
            return User.Identity.Name;
        }
        #endregion
    }
}