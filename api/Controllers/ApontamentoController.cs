using System;
using api.enums;
using api.models;
using api.services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace api.controllers
{
    public class ApontamentoController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly TokenService _tokenService;

        public ApontamentoController(IConfiguration configuration)
        {
            _configuration = configuration;
            _tokenService = new TokenService(configuration);
        }

        #region Post
        [HttpPost]
        [Authorize]
        [Route("api/add/apontamento")]
        public IActionResult Post([FromBody] Apontamento apontamento)
        {
            try
            {
                apontamento.Usuario.UsuarioId = _tokenService.GetIdToken(User);
                ApontamentoService.Add(apontamento);
                var response = ApontamentoService.ValidationResponse;
                if (response.Type.Equals("Error"))
                {
                    return StatusCode((int) StatusCodeEnum.Error, response.Message);
                }
                else
                {
                    return StatusCode((int) StatusCodeEnum.Success, new
                    {
                        Response = response
                    });
                }
            }
            catch
            {
                return BadRequest("Erro ao cadastrar apontamento.");
            }
        }
        #endregion
        #region Get
        [HttpGet]
        [Authorize]
        [Route("api/get/apontamento")]
        public IActionResult Get()
        {
            try
            {
                var usuarioId = _tokenService.GetIdToken(User);
                var apontamentos = ApontamentoService.GetAll(usuarioId);
                var response = ApontamentoService.ValidationResponse;
                if (response.Type.Equals("Error"))
                {
                    return StatusCode((int) StatusCodeEnum.Error, response.Message);
                }
                else
                {
                    return StatusCode((int) StatusCodeEnum.Success, new
                    {
                        Response = response,
                        Apontamentos = apontamentos
                    });
                }
            }
            catch
            {
                return BadRequest("Erro ao recuperar apontamentos.");
            }
        }

        [HttpGet]
        [Authorize]
        [Route("api/get/visualizarapontamento")]
        public IActionResult VisualizaApontamentos(int usuarioId, DateTime dataInicial, DateTime dataFinal)
        {
            try
            {
                var apontamentos = ApontamentoService.ObtemPorFiltro(usuarioId, dataInicial, dataFinal);
                var response = ApontamentoService.ValidationResponse;
                if (response.Type.Equals("Error"))
                {
                    return StatusCode((int) StatusCodeEnum.Error, response.Message);
                }
                else
                {
                    return StatusCode((int) StatusCodeEnum.Success, new
                    {
                        Response = response,
                        Apontamentos = apontamentos
                    });
                }
            }
            catch
            {
                return BadRequest("Erro ao recuperar apontamentos.");
            }
        }
        #endregion

        #region DELETE
        [Route("api/delete/apontamento")]
        [HttpDelete]
        public IActionResult Delete(int apontamentoId) 
        {
            ApontamentoService.Delete(apontamentoId);
            var response = ApontamentoService.ValidationResponse;
            if (response.Type.Equals("Error"))
            {
                return StatusCode((int) StatusCodeEnum.Error, response.Message);
            }
            else
            {
                return StatusCode((int) StatusCodeEnum.Success, new
                {
                    Response = response,
                });
            }
        }
        #endregion
    }
}