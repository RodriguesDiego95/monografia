using System;
using System.Linq;
using api.enums;
using api.models;
using api.services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace api.controllers
{
    public class TarefaController : Controller
    {

        private readonly IConfiguration _configuration;
        private readonly TokenService _tokenService;

        public TarefaController(IConfiguration configuration)
        {
            _configuration = configuration;
            _tokenService = new TokenService(configuration);
        }

        #region Post
        [HttpPost]
        [Authorize]
        [Route("api/add/tarefa")]
        public IActionResult Post([FromBody] Tarefa tarefa)
        {
            try
            {
                TarefaService.Add(tarefa);
                var response = TarefaService.ValidationResponse;
                if (response.Type.Equals("Error"))
                {
                    return StatusCode((int) StatusCodeEnum.Error, response.Message);
                }
                else
                {
                    return StatusCode((int) StatusCodeEnum.Success, new
                    {
                        Response = response
                    });
                }
            }
            catch
            {
                return BadRequest("Erro ao salvar tarefa.");
            }
        }
        #endregion
        #region Get
        [HttpGet]
        [Authorize]
        [Route("api/get/tarefa")]
        public IActionResult Get(int projetoId)
        {
            try
            {
                var tarefas = TarefaService.GetAll(projetoId);
                var response = TarefaService.ValidationResponse;
                if (response.Type.Equals("Error"))
                {
                    return StatusCode((int) StatusCodeEnum.Error, response.Message);
                }
                else
                {
                    return StatusCode((int) StatusCodeEnum.Success, new
                    {
                        Response = response,
                        Tarefas = tarefas
                    });
                }
            }
            catch
            {
                return BadRequest("Erro ao recuperar tarefas.");
            }
        }

        [HttpGet]
        [Authorize]
        [Route("api/get/tarefasdousuario")]
        public IActionResult GetTarefasDoUsuario()
        {
            try
            {
                var usuarioId = _tokenService.GetIdToken(User); 
                var tarefas = TarefaService.GetTarefasDoUsuario(usuarioId);
                var response = TarefaService.ValidationResponse;
                if (response.Type.Equals("Error"))
                {
                    return StatusCode((int) StatusCodeEnum.Error, response.Message);
                }
                else
                {
                    return StatusCode((int) StatusCodeEnum.Success, new
                    {
                        Response = response,
                        Tarefas = tarefas
                    });
                }
            }
            catch
            {
                return BadRequest("Erro ao recuperar tarefas.");
            }
        }
        #endregion
        #region DELETE
        [Route("api/delete/tarefa")]
        [HttpDelete]
        [Authorize]
        public IActionResult Delete(int tarefaId) 
        {
            TarefaService.Delete(tarefaId);
            var response = TarefaService.ValidationResponse;
            if (response.Type.Equals("Error"))
            {
                return StatusCode((int) StatusCodeEnum.Error, response.Message);
            }
            else
            {
                return StatusCode((int) StatusCodeEnum.Success, new
                {
                    Response = response,
                });
            }
        }
        #endregion
    }
}