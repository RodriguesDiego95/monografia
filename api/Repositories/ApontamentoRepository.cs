using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using api.models;
using MySql.Data.MySqlClient;

namespace api.repositories
{
    public class ApontamentoRepository
    {
        public void Add(Apontamento apontamento)
        {
            var query = new StringBuilder();
            query.Append(" INSERT INTO apontamento ");
            query.Append(" (                       ");
            query.Append(" descricao,              ");
            query.Append(" id_usuario,             ");
            query.Append(" id_tarefa,              ");
            query.Append(" data,                   ");
            query.Append(" hora_inicial,           ");
            query.Append(" hora_final,             ");
            query.Append(" categoria,              ");
            query.Append(" tarefa_finalizada       ");
            query.Append(" )                       ");
            query.Append(" VALUES                  ");
            query.Append(" (                       ");
            query.Append(" ?descricao,             ");
            query.Append(" ?id_usuario,            ");
            query.Append(" ?id_tarefa,             ");
            query.Append(" ?data,                  ");
            query.Append(" ?hora_inicial,          ");
            query.Append(" ?hora_final,            ");
            query.Append(" ?categoria,             ");
            query.Append(" ?tarefa_finalizada      ");
            query.Append(" )                       ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?descricao", apontamento.Descricao);
            mySqlCommand.Parameters.AddWithValue("?id_usuario", apontamento.Usuario.UsuarioId);
            mySqlCommand.Parameters.AddWithValue("?id_tarefa", apontamento.Tarefa.TarefaId);
            mySqlCommand.Parameters.AddWithValue("?data", apontamento.Data);
            mySqlCommand.Parameters.AddWithValue("?hora_inicial", apontamento.HoraInicial);
            mySqlCommand.Parameters.AddWithValue("?hora_final", apontamento.HoraFinal);
            mySqlCommand.Parameters.AddWithValue("?categoria", apontamento.Categoria);
            mySqlCommand.Parameters.AddWithValue("?tarefa_finalizada", apontamento.TarefaFinalizada);
            mySqlCommand.ExecuteNonQuery();
        }

        public void Update(Apontamento apontamento)
        {
            var query = new StringBuilder();
            query.Append(" UPDATE apontamento SET        ");
            query.Append(" descricao    = ?descricao,    ");
            query.Append(" id_tarefa    = ?id_tarefa,    ");
            query.Append(" data         = ?data,         ");
            query.Append(" hora_inicial = ?hora_inicial, ");
            query.Append(" hora_final   = ?hora_final,   ");
            query.Append(" categoria    = ?categoria,     ");
            query.Append(" tarefa_finalizada = ?tarefa_finalizada ");
            query.Append(" WHERE id     = ?id            ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id", apontamento.ApontamentoId);
            mySqlCommand.Parameters.AddWithValue("?descricao", apontamento.Descricao);
            mySqlCommand.Parameters.AddWithValue("?id_tarefa", apontamento.Tarefa.TarefaId);
            mySqlCommand.Parameters.AddWithValue("?data", apontamento.Data);
            mySqlCommand.Parameters.AddWithValue("?hora_inicial", apontamento.HoraInicial);
            mySqlCommand.Parameters.AddWithValue("?hora_final", apontamento.HoraFinal);
            mySqlCommand.Parameters.AddWithValue("?categoria", apontamento.Categoria);
            mySqlCommand.Parameters.AddWithValue("?tarefa_finalizada", apontamento.TarefaFinalizada);
            mySqlCommand.ExecuteNonQuery();
        }

        public List<Apontamento> GetAll(int usuarioId)
        {
            var dataTable = new DataTable();
            var query = new StringBuilder();
            query.Append(" SELECT * FROM apontamento   ");
            query.Append(" WHERE                       ");
            query.Append(" id_usuario = ?id_usuario    ");
            query.Append(" AND excluido = 0            ");
            query.Append(" ORDER BY data DESC, id DESC ");
            query.Append(" LIMIT 30                    ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id_usuario", usuarioId);
            dataTable.Load(mySqlCommand.ExecuteReader());
            if (dataTable.Rows.Count > 0)
            {
                var apontamentos = new List<Apontamento>();
                for (var i = 0; i < dataTable.Rows.Count; i++)
                {
                    var row = dataTable.Rows[i];
                    var apontamento = new Apontamento()
                    {
                        ApontamentoId = Convert.ToInt32(row["id"]),
                        Tarefa = new Tarefa()
                        {
                        TarefaId = Convert.ToInt32(row["id_tarefa"])
                        },
                        Descricao = row["descricao"].ToString(),
                        Data = Convert.ToDateTime(row["data"].ToString()),
                        HoraInicial = TimeSpan.Parse(row["hora_inicial"].ToString()),
                        HoraFinal = TimeSpan.Parse(row["hora_final"].ToString()),
                        Categoria = Convert.ToInt32(row["categoria"]),
                        TarefaFinalizada = Convert.ToBoolean(row["tarefa_finalizada"]),
                        Usuario = new Usuario()
                        {
                        UsuarioId = Convert.ToInt32(row["id_usuario"])
                        }
                    };
                    apontamentos.Add(apontamento);
                }
                return apontamentos;
            }
            return new List<Apontamento>();
        }

        public List<Apontamento> ObtemPorFiltro(int usuarioId, DateTime dataInicial, DateTime dataFinal)
        {
            var dataTable = new DataTable();
            var query = new StringBuilder();
            query.Append(" SELECT a.*, t.descricao AS tarefa        ");
            query.Append(" FROM apontamento a                       ");
            query.Append(" LEFT JOIN tarefa t ON t.id = a.id_tarefa ");
            query.Append(" WHERE                                    ");
            query.Append(" a.id_usuario = ?id_usuario                 ");
            query.Append(" AND a.data >= ?dataInicial                 ");
            query.Append(" AND a.data <= ?dataFinal                   ");
            query.Append(" AND a.excluido = 0                         ");
            query.Append(" ORDER BY a.data DESC, id DESC              ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id_usuario", usuarioId);
            mySqlCommand.Parameters.AddWithValue("?dataInicial", dataInicial);
            mySqlCommand.Parameters.AddWithValue("?dataFinal", dataFinal);
            dataTable.Load(mySqlCommand.ExecuteReader());
            if (dataTable.Rows.Count > 0)
            {
                var apontamentos = new List<Apontamento>();
                for (var i = 0; i < dataTable.Rows.Count; i++)
                {
                    var row = dataTable.Rows[i];
                    var apontamento = new Apontamento()
                    {
                        ApontamentoId = Convert.ToInt32(row["id"]),
                        Tarefa = new Tarefa()
                        {
                        TarefaId = Convert.ToInt32(row["id_tarefa"]),
                        Descricao = row["tarefa"].ToString()
                        },
                        Descricao = row["descricao"].ToString(),
                        Data = Convert.ToDateTime(row["data"].ToString()),
                        HoraInicial = TimeSpan.Parse(row["hora_inicial"].ToString()),
                        HoraFinal = TimeSpan.Parse(row["hora_final"].ToString()),
                        Categoria = Convert.ToInt32(row["categoria"]),
                        TarefaFinalizada = Convert.ToBoolean(row["tarefa_finalizada"]),
                        Usuario = new Usuario()
                        {
                        UsuarioId = Convert.ToInt32(row["id_usuario"])
                        }
                    };
                    apontamentos.Add(apontamento);
                }
                return apontamentos;
            }
            return new List<Apontamento>();
        }

        public bool ExisteApontamento(int tarefaId)
        {
            var dataTable = new DataTable();
            var query = new StringBuilder();
            query.Append(" SELECT * FROM apontamento ");
            query.Append(" WHERE                     ");
            query.Append(" id_tarefa = ?id_tarefa    ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id_tarefa", tarefaId);
            dataTable.Load(mySqlCommand.ExecuteReader());
            if (dataTable.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        public bool ExisteApontamentoTarefaFinalizada(int tarefaId, int apontamentoId)
        {
            var dataTable = new DataTable();
            var query = new StringBuilder();
            query.Append(" SELECT * FROM apontamento ");
            query.Append(" WHERE                     ");
            query.Append(" id_tarefa = ?id_tarefa    ");
            query.Append(" AND tarefa_finalizada = 1 ");
            query.Append(" AND id <> ?id             ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id", apontamentoId);
            mySqlCommand.Parameters.AddWithValue("?id_tarefa", tarefaId);
            dataTable.Load(mySqlCommand.ExecuteReader());
            if (dataTable.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        public bool ExisteApontamentoPorUsuario(int usuarioId)
        {
            var dataTable = new DataTable();
            var query = new StringBuilder();
            query.Append(" SELECT * FROM apontamento ");
            query.Append(" WHERE                     ");
            query.Append(" id_usuario = ?id_usuario   ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id_usuario", usuarioId);
            dataTable.Load(mySqlCommand.ExecuteReader());
            if (dataTable.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        public void Delete(int apontamentoId)
        {
            var query = new StringBuilder();
            query.Append(" DELETE FROM apontamento ");
            query.Append(" WHERE id = ?id          ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id", apontamentoId);
            mySqlCommand.ExecuteNonQuery();
        }
    }
}