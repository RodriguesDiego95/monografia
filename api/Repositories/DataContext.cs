using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;

namespace api.repositories 
{
    public class DataContext : IDisposable {
        public static MySqlConnection MySqlConnection { get; set; }
        public static MySqlTransaction MySqlTransaction { get; set; }
        private IConfiguration Configuration { get; }

        public DataContext () {
            var environmentName = Environment.GetEnvironmentVariable ("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder ()
                .SetBasePath (Directory.GetCurrentDirectory ())
                .AddJsonFile ("appsettings.json", optional : true, reloadOnChange : true)
                .AddJsonFile ($"appsettings.{environmentName}.json", optional : true);
            builder.AddEnvironmentVariables (environmentName);
            Configuration = builder.Build ();
        }

        public void BeginTransaction () {
            var connectionString = Configuration.GetConnectionString ("DefaultConnection");
            MySqlConnection = new MySqlConnection 
            {
                ConnectionString = connectionString
            };
            MySqlConnection.Open ();
            MySqlTransaction = MySqlConnection.BeginTransaction ();
        }

        public void Commit () {
            MySqlTransaction.Commit ();
        }

        public void Rollback () {
            MySqlTransaction.Rollback ();
        }

        public void Finally () {
            if (MySqlTransaction != null) {
                MySqlTransaction.Dispose ();
            }
            if (MySqlConnection != null) {
                MySqlConnection.Close ();
                MySqlConnection.Dispose ();
            }
        }

        public void Dispose () {
            GC.SuppressFinalize (this);
        }
    }
}