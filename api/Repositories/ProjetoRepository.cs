using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using api.models;
using api.repositories;
using MySql.Data.MySqlClient;

namespace api.repositories
{
    public class ProjetoRepository
    {
        public void Add(Projeto projeto)
        {
            var query = new StringBuilder();
            query.Append(" INSERT INTO projeto ");
            query.Append(" (                   ");
            query.Append(" nome,               ");
            query.Append(" descricao,          ");
            query.Append(" previsao_inicio,    ");
            query.Append(" previsao_fim        ");
            query.Append(" )                   ");
            query.Append(" VALUES              ");
            query.Append(" (                   ");
            query.Append(" ?nome,              ");
            query.Append(" ?descricao,         ");
            query.Append(" ?previsao_inicio,   ");
            query.Append(" ?previsao_fim       ");
            query.Append(" )                   ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?nome", projeto.Nome);
            mySqlCommand.Parameters.AddWithValue("?descricao", projeto.Descricao);
            mySqlCommand.Parameters.AddWithValue("?previsao_inicio", projeto.PrevisaoInicio);
            mySqlCommand.Parameters.AddWithValue("?previsao_fim", projeto.PrevisaoFim);
            mySqlCommand.ExecuteNonQuery();
        }

        public void Update(Projeto projeto)
        {
            var query = new StringBuilder();
            query.Append(" UPDATE projeto SET                           ");
            query.Append(" nome                 = ?nome,                ");
            query.Append(" descricao            = ?descricao,           ");
            query.Append(" previsao_inicio      = ?previsao_inicio,     ");
            query.Append(" previsao_fim         = ?previsao_fim,        ");
            query.Append(" previsao_tempo_gasto = ?previsao_tempo_gasto ");
            query.Append(" WHERE id             = ?id                   ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id", projeto.ProjetoId);
            mySqlCommand.Parameters.AddWithValue("?nome", projeto.Nome);
            mySqlCommand.Parameters.AddWithValue("?descricao", projeto.Descricao);
            mySqlCommand.Parameters.AddWithValue("?previsao_inicio", projeto.PrevisaoInicio);
            mySqlCommand.Parameters.AddWithValue("?previsao_fim", projeto.PrevisaoFim);
            mySqlCommand.Parameters.AddWithValue("?previsao_tempo_gasto", projeto.PrevisaoTempoGasto);
            mySqlCommand.ExecuteNonQuery();
        }

        public Projeto Get(int projetoId)
        {
            var dataTable = new DataTable();
            var query = new StringBuilder();
            query.Append(" SELECT * FROM projeto ");
            query.Append(" WHERE                 ");
            query.Append(" id = ?id ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id", projetoId);
            dataTable.Load(mySqlCommand.ExecuteReader());
            if (dataTable.Rows.Count > 0)
            {
                var row = dataTable.Rows[0];
                var projeto = new Projeto()
                {
                    ProjetoId = Convert.ToInt32(row["id"]),
                    Nome = row["nome"].ToString(),
                    Descricao = row["descricao"].ToString(),
                    PrevisaoInicio = Convert.ToDateTime(row["previsao_inicio"].ToString()),
                    PrevisaoFim = Convert.ToDateTime(row["previsao_fim"]),
                    PrevisaoTempoGasto = Convert.ToInt32(row["previsao_tempo_gasto"]),
                };
                return projeto;
            }
            return new Projeto();
        }

        public List<Projeto> GetAll()
        {
            var dataTable = new DataTable();
            var query = new StringBuilder();
            query.Append(" SELECT * FROM projeto ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            dataTable.Load(mySqlCommand.ExecuteReader());
            if (dataTable.Rows.Count > 0)
            {
                var projetos = new List<Projeto>();
                for (var i = 0; i < dataTable.Rows.Count; i++)
                {
                    var row = dataTable.Rows[i];
                    var projeto = new Projeto()
                    {
                        ProjetoId = Convert.ToInt32(row["id"]),
                        Nome = row["nome"].ToString(),
                        Descricao = row["descricao"].ToString(),
                        PrevisaoInicio = Convert.ToDateTime(row["previsao_inicio"].ToString()),
                        PrevisaoFim = Convert.ToDateTime(row["previsao_fim"]),
                        PrevisaoTempoGasto = Convert.ToInt32(row["previsao_tempo_gasto"]),
                    };
                    projetos.Add(projeto);
                }
                return projetos;
            }
            return new List<Projeto>();
        }

        public void Delete(int projetoId)
        {
            var dataTable = new DataTable();
            var query = new StringBuilder();
            query.Append(" DELETE FROM projeto ");
            query.Append(" WHERE id = ?id      ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id", projetoId);
            mySqlCommand.ExecuteNonQuery();
        }
    }
}