using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using api.models;
using MySql.Data.MySqlClient;

namespace api.repositories
{
    public class UsuarioRepository
    {
        public void Add(Usuario usuario)
        {
            var query = new StringBuilder();
            query.Append(" INSERT INTO usuario  ");
            query.Append(" (                    ");
            query.Append(" nome_completo,       ");
            query.Append(" salario,             ");
            query.Append(" valor_hora,          ");
            query.Append(" nome_usuario,        ");
            query.Append(" senha,               ");
            query.Append(" gerente              ");
            query.Append(" )                    ");
            query.Append(" VALUES               ");
            query.Append(" (                    ");
            query.Append(" ?nome_completo,      ");
            query.Append(" ?salario,            ");
            query.Append(" ?valor_hora,         ");
            query.Append(" ?nome_usuario,       ");
            query.Append(" ?senha,              ");
            query.Append(" ?gerente             ");
            query.Append(" )                    ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?nome_completo", usuario.NomeCompleto);
            mySqlCommand.Parameters.AddWithValue("?salario", usuario.Salario);
            mySqlCommand.Parameters.AddWithValue("?valor_hora", usuario.ValorHora);
            mySqlCommand.Parameters.AddWithValue("?nome_usuario", usuario.NomeUsuario);
            mySqlCommand.Parameters.AddWithValue("?senha", usuario.Senha);
            mySqlCommand.Parameters.AddWithValue("?gerente", usuario.Gerente);
            mySqlCommand.ExecuteNonQuery();
        }

        public void Update(Usuario usuario)
        {
            var query = new StringBuilder();
            query.Append(" UPDATE usuario SET              ");
            query.Append(" nome_completo = ?nome_completo, ");
            query.Append(" salario       = ?salario,       ");
            query.Append(" valor_hora    = ?valor_hora,    ");
            query.Append(" nome_usuario  = ?nome_usuario,  ");
            //query.Append(" senha         = ?senha,         ");
            query.Append(" gerente       = ?gerente        ");
            query.Append(" WHERE id      = ?id             ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id", usuario.UsuarioId);
            mySqlCommand.Parameters.AddWithValue("?nome_completo", usuario.NomeCompleto);
            mySqlCommand.Parameters.AddWithValue("?salario", usuario.Salario);
            mySqlCommand.Parameters.AddWithValue("?valor_hora", usuario.ValorHora);
            mySqlCommand.Parameters.AddWithValue("?nome_usuario", usuario.NomeUsuario);
            mySqlCommand.Parameters.AddWithValue("?senha", usuario.Senha);
            mySqlCommand.Parameters.AddWithValue("?gerente", usuario.Gerente);
            mySqlCommand.ExecuteNonQuery();
        }

        public void AlterarSenha(Usuario usuario, string senhaNova)
        {
            var query = new StringBuilder();
            query.Append(" UPDATE usuario SET    ");
            query.Append(" senha = ?senha_nova   ");
            query.Append(" WHERE                 ");
            query.Append(" id = ?id AND          ");
            query.Append(" senha = ?senha_antiga ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id", usuario.UsuarioId);
            mySqlCommand.Parameters.AddWithValue("?senha_nova", senhaNova);
            mySqlCommand.Parameters.AddWithValue("?senha_antiga", usuario.Senha);
            mySqlCommand.ExecuteNonQuery();
        }

        public List<Usuario> GetAll()
        {
            var dataTable = new DataTable();
            var query = new StringBuilder();
            query.Append(" SELECT * FROM usuario ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            dataTable.Load(mySqlCommand.ExecuteReader());
            if (dataTable.Rows.Count > 0)
            {
                var usuarios = new List<Usuario>();
                for (var i = 0; i < dataTable.Rows.Count; i++)
                {
                    var row = dataTable.Rows[i];
                    var usuario = new Usuario()
                    {
                        UsuarioId = Convert.ToInt32(row["id"]),
                        NomeCompleto = row["nome_completo"].ToString(),
                        Salario = Convert.ToDecimal(row["salario"]),
                        ValorHora = Convert.ToDecimal(row["valor_hora"]),
                        Gerente = Convert.ToBoolean(row["gerente"]),
                        NomeUsuario = row["nome_usuario"].ToString(),
                        Senha = row["senha"].ToString()
                    };
                    usuarios.Add(usuario);
                }
                return usuarios;
            }
            return new List<Usuario>();
        }

        public Usuario ObtemPorAutenticacao(string nomeUsuario, string senha)
        {
            var query = new StringBuilder();
            var dataTable = new DataTable();
            query.Append(" SELECT *                     ");
            query.Append(" FROM usuario                 ");
            query.Append(" WHERE                        ");
            query.Append(" nome_usuario = ?nome_usuario ");
            query.Append(" AND senha = ?senha           ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?nome_usuario", nomeUsuario);
            mySqlCommand.Parameters.AddWithValue("?senha", senha);
            dataTable.Load(mySqlCommand.ExecuteReader());
            if (dataTable.Rows.Count > 0)
            {
                var row = dataTable.Rows[0];
                var usuario = new Usuario()
                {
                    UsuarioId = Convert.ToInt32(row["id"]),
                    NomeCompleto = row["nome_completo"].ToString(),
                    Salario = Convert.ToDecimal(row["salario"]),
                    ValorHora = Convert.ToDecimal(row["valor_hora"]),
                    Gerente = Convert.ToBoolean(row["gerente"]),
                    NomeUsuario = row["nome_usuario"].ToString(),
                    Senha = row["senha"].ToString()
                };
                return usuario;
            }
            return new Usuario();
        }

        public Usuario ObtemPorId(int usuarioId)
        {
            var dataTable = new DataTable();
            var query = new StringBuilder();
            query.Append(" SELECT * FROM usuario ");
            query.Append(" WHERE id = ?id        ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id", usuarioId);
            dataTable.Load(mySqlCommand.ExecuteReader());
            if (dataTable.Rows.Count > 0)
            {
                var row = dataTable.Rows[0];
                var usuario = new Usuario()
                {
                    UsuarioId = Convert.ToInt32(row["id"]),
                    NomeCompleto = row["nome_completo"].ToString(),
                    ValorHora = Convert.ToDecimal(row["valor_hora"]),
                    Gerente = Convert.ToBoolean(row["gerente"]),
                    Senha = row["senha"].ToString(),
                    NomeUsuario = row["nome_usuario"].ToString()
                };
                return usuario;
            }
            return new Usuario();
        }

        public void Delete(int usuarioId)
        {
            var query = new StringBuilder();
            query.Append(" DELETE FROM usuario ");
            query.Append(" WHERE id = ?id      ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id", usuarioId);
            mySqlCommand.ExecuteNonQuery();
        }
    }
}