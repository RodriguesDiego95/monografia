using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using api.models;
using MySql.Data.MySqlClient;

namespace api.repositories
{
    public class TarefaRepository
    {
        public void Add(Tarefa tarefa)
        {
            var query = new StringBuilder();
            query.Append(" INSERT INTO tarefa    ");
            query.Append(" (                     ");
            query.Append(" descricao,            ");
            query.Append(" id_projeto,           ");
            query.Append(" id_usuario,           ");
            query.Append(" previsao_inicio,      ");
            query.Append(" previsao_fim,         ");
            query.Append(" previsao_tempo_gasto  ");
            query.Append(" )                     ");
            query.Append(" VALUES                ");
            query.Append(" (                     ");
            query.Append(" ?descricao,           ");
            query.Append(" ?id_projeto,          ");
            query.Append(" ?id_usuario,          ");
            query.Append(" ?previsao_inicio,     ");
            query.Append(" ?previsao_fim,        ");
            query.Append(" ?previsao_tempo_gasto ");
            query.Append(" )                     ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?descricao", tarefa.Descricao);
            mySqlCommand.Parameters.AddWithValue("?id_projeto", tarefa.Projeto.ProjetoId);
            mySqlCommand.Parameters.AddWithValue("?id_usuario", tarefa.Usuario.UsuarioId);
            mySqlCommand.Parameters.AddWithValue("?previsao_inicio", tarefa.PrevisaoInicio);
            mySqlCommand.Parameters.AddWithValue("?previsao_fim", tarefa.PrevisaoFim);
            mySqlCommand.Parameters.AddWithValue("?previsao_tempo_gasto", tarefa.PrevisaoTempoGasto);
            mySqlCommand.ExecuteNonQuery();
        }

        public void Update(Tarefa tarefa)
        {
            var query = new StringBuilder();
            query.Append(" UPDATE tarefa SET                             ");
            query.Append(" descricao            = ?descricao,            ");
            query.Append(" previsao_inicio      = ?previsao_inicio,      ");
            query.Append(" previsao_fim         = ?previsao_fim,         ");
            query.Append(" previsao_tempo_gasto = ?previsao_tempo_gasto, ");
            query.Append(" id_usuario           = ?id_usuario            ");
            query.Append(" WHERE id             = ?id                    ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id", tarefa.TarefaId);
            mySqlCommand.Parameters.AddWithValue("?descricao", tarefa.Descricao);
            mySqlCommand.Parameters.AddWithValue("?previsao_inicio", tarefa.PrevisaoInicio);
            mySqlCommand.Parameters.AddWithValue("?previsao_fim", tarefa.PrevisaoFim);
            mySqlCommand.Parameters.AddWithValue("?previsao_tempo_gasto", tarefa.PrevisaoTempoGasto);
            mySqlCommand.Parameters.AddWithValue("?id_usuario", tarefa.Usuario.UsuarioId);
            mySqlCommand.ExecuteNonQuery();
        }

        public Tarefa Get(int tarefaId)
        {
            var dataTable = new DataTable();
            var query = new StringBuilder();
            query.Append(" SELECT * FROM tarefa     ");
            query.Append(" WHERE                    ");
            query.Append(" id = ?id ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id", tarefaId);
            dataTable.Load(mySqlCommand.ExecuteReader());
            if (dataTable.Rows.Count > 0)
            {
                var row = dataTable.Rows[0];
                var tarefa = new Tarefa()
                {
                    TarefaId = Convert.ToInt32(row["id"]),
                    Projeto = new Projeto()
                    {
                    ProjetoId = Convert.ToInt32(row["id_projeto"])
                    },
                    Descricao = row["descricao"].ToString(),
                    PrevisaoInicio = Convert.ToDateTime(row["previsao_inicio"].ToString()),
                    PrevisaoFim = Convert.ToDateTime(row["previsao_fim"]),
                    PrevisaoTempoGasto = Convert.ToInt32(row["previsao_tempo_gasto"]),
                    Usuario = new Usuario()
                    {
                    UsuarioId = Convert.ToInt32(row["id_usuario"])
                    }
                };
                return tarefa;
            }
            return new Tarefa();
        }

        public List<Tarefa> GetAll(int projetoId)
        {
            var dataTable = new DataTable();
            var query = new StringBuilder();
            query.Append(" SELECT * FROM tarefa     ");
            query.Append(" WHERE                    ");
            query.Append(" id_projeto = ?id_projeto ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id_projeto", projetoId);
            dataTable.Load(mySqlCommand.ExecuteReader());
            if (dataTable.Rows.Count > 0)
            {
                var tarefas = new List<Tarefa>();
                for (var i = 0; i < dataTable.Rows.Count; i++)
                {
                    var row = dataTable.Rows[i];
                    var tarefa = new Tarefa()
                    {
                        TarefaId = Convert.ToInt32(row["id"]),
                        Projeto = new Projeto()
                        {
                        ProjetoId = Convert.ToInt32(row["id_projeto"])
                        },
                        Descricao = row["descricao"].ToString(),
                        PrevisaoInicio = Convert.ToDateTime(row["previsao_inicio"].ToString()),
                        PrevisaoFim = Convert.ToDateTime(row["previsao_fim"]),
                        PrevisaoTempoGasto = Convert.ToInt32(row["previsao_tempo_gasto"]),
                        Usuario = new Usuario()
                        {
                        UsuarioId = Convert.ToInt32(row["id_usuario"])
                        }
                    };
                    tarefas.Add(tarefa);
                }
                return tarefas;
            }
            return new List<Tarefa>();
        }

        public bool ExisteTarefa(int projetoId)
        {
            var dataTable = new DataTable();
            var query = new StringBuilder();
            query.Append(" SELECT * FROM tarefa     ");
            query.Append(" WHERE                    ");
            query.Append(" id_projeto = ?id_projeto ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id_projeto", projetoId);
            dataTable.Load(mySqlCommand.ExecuteReader());
            if (dataTable.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        public bool ExisteTarefaPorUsuario(int usuarioId)
        {
            var dataTable = new DataTable();
            var query = new StringBuilder();
            query.Append(" SELECT * FROM tarefa     ");
            query.Append(" WHERE                    ");
            query.Append(" id_usuario = ?id_usuario ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id_usuario", usuarioId);
            dataTable.Load(mySqlCommand.ExecuteReader());
            if (dataTable.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        public List<Tarefa> GetTarefasDoUsuario(int usuarioId)
        {
            var dataTable = new DataTable();
            var query = new StringBuilder();
            query.Append(" SELECT * FROM tarefa     ");
            query.Append(" WHERE                    ");
            query.Append(" id_usuario = ?id_usuario ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id_usuario", usuarioId);
            dataTable.Load(mySqlCommand.ExecuteReader());
            if (dataTable.Rows.Count > 0)
            {
                var tarefas = new List<Tarefa>();
                for (var i = 0; i < dataTable.Rows.Count; i++)
                {
                    var row = dataTable.Rows[i];
                    var tarefa = new Tarefa()
                    {
                        TarefaId = Convert.ToInt32(row["id"]),
                        Projeto = new Projeto()
                        {
                        ProjetoId = Convert.ToInt32(row["id_projeto"])
                        },
                        Descricao = row["descricao"].ToString(),
                        PrevisaoInicio = Convert.ToDateTime(row["previsao_inicio"].ToString()),
                        PrevisaoFim = Convert.ToDateTime(row["previsao_fim"]),
                        PrevisaoTempoGasto = Convert.ToInt32(row["previsao_tempo_gasto"]),
                        Usuario = new Usuario()
                        {
                        UsuarioId = Convert.ToInt32(row["id_usuario"])
                        }
                    };
                    tarefas.Add(tarefa);
                }
                return tarefas;
            }
            return new List<Tarefa>();
        }

        public List<Tarefa> GetTarefasCusto(int projetoId)
        {
            var dataTable = new DataTable();
            var query = new StringBuilder();
            query.Append(" SELECT                                                    ");
            query.Append(" t.*,                                                      ");
            query.Append(" COALESCE((t.previsao_tempo_gasto * u.valor_hora),0) AS valor_estimado ");
            query.Append(" FROM tarefa t                                             ");
            query.Append(" LEFT JOIN usuario u ON t.id_usuario=u.id                  ");
            query.Append(" WHERE                                                     ");
            query.Append(" id_projeto = ?id_projeto                                  ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id_projeto", projetoId);
            dataTable.Load(mySqlCommand.ExecuteReader());
            if (dataTable.Rows.Count > 0)
            {
                var tarefas = new List<Tarefa>();
                for (var i = 0; i < dataTable.Rows.Count; i++)
                {
                    var row = dataTable.Rows[i];
                    var tarefa = new Tarefa()
                    {
                        TarefaId = Convert.ToInt32(row["id"]),
                        Projeto = new Projeto()
                        {
                        ProjetoId = Convert.ToInt32(row["id_projeto"])
                        },
                        Descricao = row["descricao"].ToString(),
                        PrevisaoInicio = Convert.ToDateTime(row["previsao_inicio"].ToString()),
                        PrevisaoFim = Convert.ToDateTime(row["previsao_fim"]),
                        PrevisaoTempoGasto = Convert.ToInt32(row["previsao_tempo_gasto"]),
                        Usuario = new Usuario()
                        {
                        UsuarioId = Convert.ToInt32(row["id_usuario"])
                        },
                        ValorEstimado = Convert.ToDecimal(row["valor_estimado"])
                    };
                    tarefas.Add(tarefa);
                }
                return tarefas;
            }
            return new List<Tarefa>();
        }

        public void Delete(int tarefaId)
        {
            var dataTable = new DataTable();
            var query = new StringBuilder();
            query.Append(" DELETE FROM tarefa  ");
            query.Append(" WHERE id = ?id      ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id", tarefaId);
            mySqlCommand.ExecuteNonQuery();
        }
    }
}