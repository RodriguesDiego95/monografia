using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using api.models;
using MySql.Data.MySqlClient;

namespace api.repositories
{
    public class ResultadoRepository
    {
        public List<ResultadoCategoria> GetAllResultadoCategoria(
            DateTime dataInicial, DateTime dataFinal)
        {
            var dataTable = new DataTable();
            var query = new StringBuilder();
            query.Append(" SELECT                                                                               ");
            query.Append(" id_usuario AS cod_usuario, usuario.nome_completo, usuario.valor_hora,                ");
            query.Append(" (                                                                                    ");
            query.Append("  SELECT COALESCE(TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(hora_final,hora_inicial)))),'%H:%i:%s'),'00:00:00') ");
            query.Append("  FROM apontamento                                                                    ");
            query.Append("  WHERE id_usuario = cod_usuario AND categoria = 1 AND                                ");
            query.Append("  data >= ?data_inicial AND data <= ?data_final                                       ");
            query.Append(" ) AS tempo_produtividade,                                                            ");

            query.Append(" (                                                                                    ");
            query.Append("  select COALESCE(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial))),0) / 3600       ");
            query.Append("  FROM apontamento                                                                    ");
            query.Append("  WHERE id_usuario = cod_usuario AND categoria = 1 AND                                ");
            query.Append("  data >= ?data_inicial AND data <= ?data_final                                       ");
            query.Append(" ) AS horas_produtividade,                                                            ");

            query.Append(" (                                                                                    ");
            query.Append("  SELECT COALESCE(TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(hora_final,hora_inicial)))),'%H:%i:%s'),'00:00:00') ");
            query.Append("  FROM apontamento                                                                    ");
            query.Append("  WHERE id_usuario = cod_usuario AND categoria = 2 AND                                ");
            query.Append("  data >= ?data_inicial AND data <= ?data_final                                       ");
            query.Append(" ) AS tempo_investimento,                                                             ");

            query.Append(" (                                                                                    ");
            query.Append("  select COALESCE(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial))),0) / 3600       ");
            query.Append("  FROM apontamento                                                                    ");
            query.Append("  WHERE id_usuario = cod_usuario AND categoria = 2 AND                                ");
            query.Append("  data >= ?data_inicial AND data <= ?data_final                                       ");
            query.Append(" ) AS horas_investimento,                                                             ");

            query.Append(" (                                                                                    ");
            query.Append("  SELECT COALESCE(TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(TIMEDIFF(hora_final,hora_inicial)))),'%H:%i:%s'),'00:00:00') ");
            query.Append("  FROM apontamento                                                                    ");
            query.Append("  WHERE id_usuario = cod_usuario AND categoria = 3 AND                                ");
            query.Append("  data >= ?data_inicial AND data <= ?data_final                                       ");
            query.Append(" ) AS tempo_perda,                                                                    ");

            query.Append(" (                                                                                    ");
            query.Append("  select COALESCE(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial))),0) / 3600       ");
            query.Append("  FROM apontamento                                                                    ");
            query.Append("  WHERE id_usuario = cod_usuario AND categoria = 3 AND                                ");
            query.Append("  data >= ?data_inicial AND data <= ?data_final                                       ");
            query.Append(" ) AS horas_perda                                                                     ");

            query.Append(" FROM apontamento                                                                     ");
            query.Append(" LEFT JOIN usuario ON usuario.id = apontamento.id_usuario                             ");
            query.Append(" WHERE data >= ?data_inicial AND data <= ?data_final                                  ");
            query.Append(" GROUP BY id_usuario                                                                  ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?data_inicial", dataInicial.Date);
            mySqlCommand.Parameters.AddWithValue("?data_final", dataFinal.Date);
            dataTable.Load(mySqlCommand.ExecuteReader());
            if (dataTable.Rows.Count > 0)
            {
                var resultadosCategoria = new List<ResultadoCategoria>();
                for (var i = 0; i < dataTable.Rows.Count; i++)
                {
                    var row = dataTable.Rows[i];
                    var resultadoCategoria = new ResultadoCategoria()
                    {
                        Usuario = new Usuario()
                        {
                        UsuarioId = Convert.ToInt32(row["cod_usuario"]),
                        NomeCompleto = row["nome_completo"].ToString(),
                        ValorHora = Convert.ToDecimal(row["valor_hora"])
                        },
                        TextoProdutividade = row["tempo_produtividade"].ToString(),
                        TextoInvestimento = row["tempo_investimento"].ToString(),
                        TextoPerda = row["tempo_perda"].ToString(),
                        HorasProdutividade = Convert.ToDecimal(row["horas_produtividade"]),
                        HorasInvestimento = Convert.ToDecimal(row["horas_investimento"]),
                        HorasPerda = Convert.ToDecimal(row["horas_perda"]),
                        // TotalProdutividade = TimeSpan.Parse(row["tempo_produtividade"].ToString()),
                        // TotalInvestimento = TimeSpan.Parse(row["tempo_investimento"].ToString()),
                        // TotalPerda = TimeSpan.Parse(row["tempo_perda"].ToString())
                    };
                    resultadosCategoria.Add(resultadoCategoria);
                }
                return resultadosCategoria;
            }
            return new List<ResultadoCategoria>();
        }

        public List<ResultadoDesempenho> GetAllResultadoDesempenho(int projetoId)
        {
            var dataTable = new DataTable();
            var query = new StringBuilder();
            query.Append(" SELECT                                                                                   ");
            query.Append(" usuario.nome_completo AS colaborador, usuario.id AS cod_colaborador, usuario.valor_hora, ");
            query.Append(" projeto.nome AS nome_projeto,                                                             ");
            query.Append(" COALESCE(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial))),0) / 3600 as tempo_gasto,   ");
            query.Append(" COALESCE(TIME_FORMAT(SEC_TO_TIME(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial)))),'%H:%i:%s'),'00:00:00') as texto_tempo, ");

            query.Append(" ( ");
            query.Append("  SELECT sum(previsao_tempo_gasto) FROM tarefa t                 ");
            query.Append("  WHERE t.id_usuario = usuario.id AND t.id_projeto = ?id_projeto ");
            query.Append("  LIMIT 1                                                        ");
            query.Append(" ) AS soma_previsao_tempo_gasto,                                 ");

            query.Append(" ( ");
            query.Append("  SELECT COALESCE(sum(previsao_tempo_gasto),0) FROM tarefa t               ");
            query.Append("  LEFT JOIN apontamento a ON t.id = a.id_tarefa AND a.tarefa_finalizada=1  ");
            query.Append("  WHERE t.id_projeto = ?id_projeto AND a.id_usuario=usuario.id LIMIT 1     ");
            query.Append(" ) AS soma_previsao_tarefas_finalizadas                                    ");

            query.Append(" FROM usuario                                                 ");
            query.Append(" LEFT JOIN apontamento ON apontamento.id_usuario = usuario.id ");
            query.Append(" LEFT JOIN tarefa ON apontamento.id_tarefa = tarefa.id        ");
            query.Append(" LEFT JOIN projeto ON projeto.id = tarefa.id_projeto          ");
            query.Append(" WHERE projeto.id = ?id_projeto                               ");
            query.Append(" GROUP BY colaborador                                         ");
            query.Append(" ORDER BY colaborador                                         ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("id_projeto", projetoId);
            dataTable.Load(mySqlCommand.ExecuteReader());
            if (dataTable.Rows.Count > 0)
            {
                var resultadosDesempenho = new List<ResultadoDesempenho>();
                for (var i = 0; i < dataTable.Rows.Count; i++)
                {
                    var row = dataTable.Rows[i];
                    var resultadoDesempenho = new ResultadoDesempenho()
                    {
                        Usuario = new Usuario()
                        {
                        UsuarioId = Convert.ToInt32(row["cod_colaborador"]),
                        NomeCompleto = row["colaborador"].ToString(),
                        ValorHora = Convert.ToDecimal(row["valor_hora"])
                        },
                        Projeto = new Projeto()
                        {
                        ProjetoId = projetoId,
                        Nome = row["nome_projeto"].ToString()
                        },
                        TempoGasto = Convert.ToDecimal(row["tempo_gasto"]),
                        TextoTempoGasto = row["texto_tempo"].ToString(),
                        SomaPrevisaoTempo = Convert.ToInt32(row["soma_previsao_tempo_gasto"]),
                        SomaPrevisaoTarefasFinalizadas = Convert.ToInt32(row["soma_previsao_tarefas_finalizadas"])
                    };
                    resultadosDesempenho.Add(resultadoDesempenho);
                }
                return resultadosDesempenho;
            }
            return new List<ResultadoDesempenho>();
        }

        public ResultadoProjeto GetAllResultadoProjeto(int projetoId)
        {
            var dataTable = new DataTable();
            var query = new StringBuilder();
            query.Append(" SELECT     ");
            query.Append(" projeto.id AS codigo_projeto, projeto.nome AS nome_projeto, ");
            query.Append(" COALESCE(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial))),0) / 3600 as tempo_gasto, ");
            query.Append(" COALESCE(time_format(SEC_TO_TIME(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial)))),'%H:%i:%s'),'00:00:00') as texto_tempo_gasto, ");
            query.Append(" COALESCE(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial)) * (valor_hora / 3600) ),0) as custo_realizado, ");

            query.Append(" ( ");
            query.Append("  SELECT SUM(COALESCE((t.previsao_tempo_gasto * u.valor_hora),0)) FROM tarefa t ");
            query.Append("  LEFT JOIN usuario u ON t.id_usuario=u.id WHERE id_projeto = ?id_projeto ");
            query.Append(" ) AS estimativa_custo, ");

            query.Append(" ( ");
            query.Append("  SELECT SUM(COALESCE(t.previsao_tempo_gasto,0)) FROM tarefa t ");
            query.Append("  WHERE id_projeto = ?id_projeto                               ");
            query.Append(" ) AS soma_estimativa_tempo,                                   ");

            query.Append(" ( ");
            query.Append("  SELECT COALESCE(sum(previsao_tempo_gasto),0) FROM tarefa t LEFT JOIN apontamento a ON t.id = a.id_tarefa ");
            query.Append("  WHERE t.id_projeto = ?id_projeto AND a.tarefa_finalizada = 1                                             ");
            query.Append(" ) AS soma_estimativa_tarefas_finalizadas                                                                  ");

            query.Append(" FROM apontamento ");
            query.Append(" LEFT JOIN tarefa ON tarefa.id = apontamento.id_tarefa    ");
            query.Append(" LEFT JOIN projeto ON projeto.id = tarefa.id_projeto      ");
            query.Append(" LEFT JOIN usuario ON usuario.id = apontamento.id_usuario ");
            query.Append(" WHERE tarefa.id_projeto = ?id_projeto                    ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("?id_projeto", projetoId);
            dataTable.Load(mySqlCommand.ExecuteReader());
            if (dataTable.Rows.Count > 0)
            {
                var row = dataTable.Rows[0];
                var resultadoProjeto = new ResultadoProjeto()
                {
                    Projeto = new Projeto()
                    {
                        ProjetoId = Convert.ToInt32(row["codigo_projeto"]),
                        Nome = row["nome_projeto"].ToString()
                    },
                    TempoGasto = Convert.ToDecimal(row["tempo_gasto"]),
                    TextoTempoGasto = row["texto_tempo_gasto"].ToString(),
                    CustoRealizado = Convert.ToDecimal(row["custo_realizado"]),
                    SomaEstimativaTempo = Convert.ToInt32(row["soma_estimativa_tempo"]),
                    EstimativaCusto = Convert.ToDecimal(row["estimativa_custo"]),
                    SomaEstimativaTarefasFinalizadas = Convert.ToInt32(row["soma_estimativa_tarefas_finalizadas"])
                };
                return resultadoProjeto;
            }
            return new ResultadoProjeto();
        }

        public List<ResultadoTarefa> GetAllResultadoTarefas(int projetoId)
        {
            var dataTable = new DataTable();
            var query = new StringBuilder();
            query.Append(" SELECT                                                                                                         ");
            query.Append(" tarefa.id, tarefa.descricao as tarefa, usuario.nome_completo AS colaborador,                                   ");
            query.Append(" COALESCE(usuario.valor_hora,0) AS valor_hora, tarefa.previsao_tempo_gasto, tarefa.previsao_fim,                ");
            query.Append(" COALESCE(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial))),0) / 3600 as tempo_gasto,                         ");
            query.Append(" COALESCE(TIME_FORMAT(SEC_TO_TIME(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial)))),'%H:%i:%s'),'00:00:00') as texto_tempo_gasto,    ");

            query.Append(" COALESCE( ");
            query.Append("   ( ");
            query.Append("    SELECT tarefa_finalizada FROM apontamento a WHERE a.id_tarefa = tarefa.id ORDER BY tarefa_finalizada DESC LIMIT 1 ");
            query.Append("   ),0                                                                                                                ");
            query.Append("   ) AS tarefa_finalizada,                                                                                            ");

            query.Append(" ( ");
            query.Append("  SELECT data FROM apontamento a WHERE a.id_tarefa = tarefa.id AND tarefa_finalizada=1 ORDER BY tarefa_finalizada DESC LIMIT 1 ");
            query.Append(" ) AS data_finalizacao                                                                                             ");

            query.Append(" FROM tarefa                                                ");
            query.Append(" LEFT JOIN apontamento ON apontamento.id_tarefa = tarefa.id ");
            query.Append(" LEFT JOIN usuario ON tarefa.id_usuario = usuario.id        ");
            query.Append(" WHERE tarefa.id_projeto = ?id_projeto                      ");
            query.Append(" GROUP BY colaborador,id_tarefa                             ");
            query.Append(" ORDER BY tarefa.previsao_fim                               ");
            var mySqlCommand = new MySqlCommand(
                query.ToString(), DataContext.MySqlConnection, DataContext.MySqlTransaction);
            mySqlCommand.Parameters.AddWithValue("id_projeto", projetoId);
            dataTable.Load(mySqlCommand.ExecuteReader());
            if (dataTable.Rows.Count > 0)
            {
                var resultadosTarefas = new List<ResultadoTarefa>();
                for (var i = 0; i < dataTable.Rows.Count; i++)
                {
                    var row = dataTable.Rows[i];
                    var resultadoTarefa = new ResultadoTarefa()
                    {
                        Usuario = new Usuario()
                        {
                        NomeCompleto = row["colaborador"].ToString(),
                        ValorHora = Convert.ToDecimal(row["valor_hora"])
                        },
                        Tarefa = new Tarefa() 
                        {
                            Descricao = row["tarefa"].ToString()
                        },
                        TempoGasto = Convert.ToDecimal(row["tempo_gasto"]),
                        TextoTempoGasto = row["texto_tempo_gasto"].ToString(),
                        SomaPrevisaoTempo = Convert.ToInt32(row["previsao_tempo_gasto"]),
                        TarefaFinalizada = Convert.ToBoolean(row["tarefa_finalizada"]),
                        DataFinalizacao = row["data_finalizacao"].ToString(),
                        PrevisaoFim = Convert.ToDateTime(row["previsao_fim"].ToString()),
                    };
                    resultadosTarefas.Add(resultadoTarefa);
                }
                return resultadosTarefas;
            }
            return new List<ResultadoTarefa>();
        }
    }
}