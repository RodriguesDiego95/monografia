using System;
using System.Collections.Generic;
using api.enums;
using api.models;
using api.repositories;

namespace api.services
{
    public static class TarefaService
    {
        private static readonly TarefaRepository _tarefaRepository = new TarefaRepository();
        private static readonly ApontamentoRepository _apontamentoRepository = new ApontamentoRepository();
        private static ValidationResponse _validationResponse = new ValidationResponse();

        public static ValidationResponse ValidationResponse
        {
            get
            {
                return _validationResponse ?? new ValidationResponse();
            }
        }

        public static void Add(Tarefa tarefa)
        {
            using (var dataContext = new DataContext())
            {
                try
                {
                    dataContext.BeginTransaction();
                    if (ValidaTarefa(tarefa))
                    {
                        if (tarefa.TarefaId > 0) 
                        {
                            _tarefaRepository.Update(tarefa);
                        } 
                        else 
                        {
                            _tarefaRepository.Add(tarefa);
                        }
                        dataContext.Commit();
                        ValidationResponse.SetMessageAndType("Tarefa salva com sucesso.", ResponseTypeEnum.Success, (int)StatusCodeEnum.Success);
                    }
                }
                catch
                {
                    dataContext.Rollback();
                    ValidationResponse.SetMessageAndType("Erro ao salvar tarefa.", ResponseTypeEnum.Error, (int)StatusCodeEnum.Error);
                }
                finally
                {
                    dataContext.Finally();
                }
            }
        }

        public static List<Tarefa> GetAll(int projetoId) 
        {
            using (var dataContext = new DataContext())
            {
                try
                {
                    dataContext.BeginTransaction();
                    var tarefas = _tarefaRepository.GetTarefasCusto(projetoId);
                    ValidationResponse.SetMessageAndType("Tarefas recuperadas com sucesso.", ResponseTypeEnum.Success, (int)StatusCodeEnum.Success);
                    return tarefas;
                }
                catch
                {
                    dataContext.Rollback();
                    ValidationResponse.SetMessageAndType("Erro ao recuperar tarefas.", ResponseTypeEnum.Error,(int)StatusCodeEnum.Error);
                    return new List<Tarefa>();
                }
                finally
                {
                    dataContext.Finally();
                }
            }
        }

        public static List<Tarefa> GetTarefasDoUsuario(int usuarioId) 
        {
            using (var dataContext = new DataContext())
            {
                try
                {
                    dataContext.BeginTransaction();
                    var tarefas = _tarefaRepository.GetTarefasDoUsuario(usuarioId);
                    ValidationResponse.SetMessageAndType("Tarefas recuperadas com sucesso.", ResponseTypeEnum.Success, (int)StatusCodeEnum.Success);
                    return tarefas;
                }
                catch
                {
                    dataContext.Rollback();
                    ValidationResponse.SetMessageAndType("Erro ao recuperar tarefas.", ResponseTypeEnum.Error,(int)StatusCodeEnum.Error);
                    return new List<Tarefa>();
                }
                finally
                {
                    dataContext.Finally();
                }
            }
        }
        
        private static bool ValidaTarefa(Tarefa tarefa)
        {
            ValidationResponse.SetDefaultValues();
            if (string.IsNullOrEmpty(tarefa.Descricao))
            {
                ValidationResponse.AddValidationError("descricao", "Informe a descrição da tarefa.");
            }
            if (tarefa.Projeto.ProjetoId == 0)
            {
                ValidationResponse.AddValidationError("projeto", "Informe em qual projeto a tarefa pertence.");
            }
            if (tarefa.PrevisaoInicio == new DateTime())
            {
                ValidationResponse.AddValidationError("previsao_inicio", "Informe a previsão para o início da tarefa.");
            }
            if (tarefa.PrevisaoFim == new DateTime())
            {
                ValidationResponse.AddValidationError("previsao_fim", "Informe a previsão para o final da tarefa.");
            }
            if (tarefa.PrevisaoTempoGasto == 0)
            {
                ValidationResponse.AddValidationError("previsao_tempo_gasto", "Informe a previsão para o tempo gasto.");
            }
            if (ValidationResponse.ValidationErrors.Count > 0)
            {
                ValidationResponse.SetMessageAndType("Informe os dados do projeto corretamente.", ResponseTypeEnum.Warning, (int)StatusCodeEnum.Warning);
            }
            return ValidationResponse.Type != ResponseTypeEnum.Warning;
        }

        public static void Delete(int tarefaId)
        {
            using(var dataContext = new DataContext())
            {
                try
                {
                    dataContext.BeginTransaction();
                    if (ValidaExclusao(tarefaId))
                    {
                        _tarefaRepository.Delete(tarefaId);
                        dataContext.Commit();
                        ValidationResponse.SetMessageAndType("Tarefa excluída com sucesso.", ResponseTypeEnum.Success, (int) StatusCodeEnum.Success);
                    }
                }
                catch
                {
                    dataContext.Rollback();
                    ValidationResponse.SetMessageAndType("Erro ao excluir tarefa.", ResponseTypeEnum.Error, (int) StatusCodeEnum.Error);
                }
                finally
                {
                    dataContext.Finally();
                }
            }
        }

        private static bool ValidaExclusao(int tarefaId)
        {
            ValidationResponse.SetDefaultValues();
            var tarefa = _tarefaRepository.Get(tarefaId);
            var mensagem = string.Empty;
            if (tarefa.TarefaId == 0)
            {
                ValidationResponse.AddValidationError("projeto_id", "Tarefa não encontrado.");
                mensagem = "Tarefa não encontrada.";
            }
            else if (_apontamentoRepository.ExisteApontamento(tarefaId))
            {
                ValidationResponse.AddValidationError("tarefa", "Existe apontamento com a tarefa.");
                mensagem = "Existe apontamento com a tarefa.";
            }
            if (ValidationResponse.ValidationErrors.Count > 0)
            {
                ValidationResponse.SetMessageAndType("Não foi possível realizar a exclusão! " + mensagem, ResponseTypeEnum.Warning, (int) StatusCodeEnum.Warning);
            }
            return ValidationResponse.Type != ResponseTypeEnum.Warning;
        }
    }
}