using System;
using System.Collections.Generic;
using api.enums;
using api.models;
using api.repositories;

namespace api.services
{
    public class UsuarioService
    {
    private static readonly UsuarioRepository _usuarioRepository = new UsuarioRepository();
    private static readonly ApontamentoRepository _apontamentoRepository = new ApontamentoRepository();
    private static readonly TarefaRepository _tarefaRepository = new TarefaRepository();
    private static ValidationResponse _validationResponse = new ValidationResponse();

    public static ValidationResponse ValidationResponse
    {
    get
    {
    return _validationResponse ?? new ValidationResponse();
            }
        }

        public static void Add(Usuario usuario)
        {
            using(var dataContext = new DataContext())
            {
                try
                {
                    dataContext.BeginTransaction();
                    if (ValidaUsuario(usuario))
                    {
                        if (usuario.UsuarioId > 0)
                        {
                            _usuarioRepository.Update(usuario);
                        }
                        else
                        {
                            usuario.Senha = HashMD5Service.Encrypt(usuario.Senha);
                            _usuarioRepository.Add(usuario);
                        }
                        dataContext.Commit();
                        ValidationResponse.SetMessageAndType("Usuário cadastrado com sucesso.", ResponseTypeEnum.Success, (int) StatusCodeEnum.Success);
                    }
                }
                catch
                {
                    dataContext.Rollback();
                    ValidationResponse.SetMessageAndType("Erro ao cadastrar usuário.", ResponseTypeEnum.Error, (int) StatusCodeEnum.Error);
                }
                finally
                {
                    dataContext.Finally();
                }
            }
        }

        public static List<Usuario> GetAll()
        {
            using(var dataContext = new DataContext())
            {
                try
                {
                    dataContext.BeginTransaction();
                    var usuarios = _usuarioRepository.GetAll();
                    ValidationResponse.SetMessageAndType("Usuarios recuperados com sucesso.", ResponseTypeEnum.Success, (int) StatusCodeEnum.Success);
                    return usuarios;
                }
                catch
                {
                    dataContext.Rollback();
                    ValidationResponse.SetMessageAndType("Erro ao recuperar usuarios.", ResponseTypeEnum.Error, (int) StatusCodeEnum.Error);
                    return new List<Usuario>();
                }
                finally
                {
                    dataContext.Finally();
                }
            }
        }

        public static Usuario Get(string nomeUsuario, string senha)
        {
            using(var dataContext = new DataContext())
            {
                try
                {
                    dataContext.BeginTransaction();
                    var usuario = _usuarioRepository.ObtemPorAutenticacao(nomeUsuario, HashMD5Service.Encrypt(senha));
                    if (usuario.UsuarioId == 0)
                    {
                        ValidationResponse.SetMessageAndType("Usuário não encontrado.", ResponseTypeEnum.Error, (int) StatusCodeEnum.Error);
                        return new Usuario();
                    }
                    else
                    {
                        ValidationResponse.SetMessageAndType("Usuário recuperado com sucesso.", ResponseTypeEnum.Success, (int) StatusCodeEnum.Success);
                        return usuario;
                    }
                }
                catch
                {
                    dataContext.Rollback();
                    ValidationResponse.SetMessageAndType("Erro ao recuperar usuário.", ResponseTypeEnum.Error, (int) StatusCodeEnum.Error);
                    return new Usuario();
                }
                finally
                {
                    dataContext.Finally();
                }
            }
        }

        public static Usuario ObtemPorId(int usuarioId)
        {
            using(var dataContext = new DataContext())
            {
                try
                {
                    ValidationResponse.SetDefaultValues();
                    dataContext.BeginTransaction();
                    var usuario = _usuarioRepository.ObtemPorId(usuarioId);
                    if (usuario.UsuarioId == 0)
                    {
                        ValidationResponse.SetMessageAndType("Usuário não encontrado.", ResponseTypeEnum.Error, (int) StatusCodeEnum.Error);
                        return new Usuario();
                    }
                    else
                    {
                        ValidationResponse.SetMessageAndType("Usuário recuperado com sucesso.", ResponseTypeEnum.Success, (int) StatusCodeEnum.Success);
                        return usuario;
                    }
                }
                catch
                {
                    ValidationResponse.SetMessageAndType("Erro ao recuperar usuário.", ResponseTypeEnum.Error, (int) StatusCodeEnum.Error);
                    return new Usuario();
                }
                finally
                {
                    dataContext.Finally();
                }
            }
        }

        public static void Delete(int usuarioId)
        {
            using(var dataContext = new DataContext())
            {
                try
                {
                    dataContext.BeginTransaction();
                    if (ValidaExclusao(usuarioId))
                    {
                        _usuarioRepository.Delete(usuarioId);
                        dataContext.Commit();
                        ValidationResponse.SetMessageAndType("Usuário excluído com sucesso.", ResponseTypeEnum.Success, (int) StatusCodeEnum.Success);
                    }
                }
                catch
                {
                    dataContext.Rollback();
                    ValidationResponse.SetMessageAndType("Erro ao excluir usuário.", ResponseTypeEnum.Error, (int) StatusCodeEnum.Error);
                }
                finally
                {
                    dataContext.Finally();
                }
            }
        }

        public static void AlterarSenha(int usuarioId, string senhaAntiga, string senhaNova)
        {
            using(var dataContext = new DataContext())
            {
                try
                {
                    dataContext.BeginTransaction();
                    senhaAntiga = HashMD5Service.Encrypt(senhaAntiga);
                    senhaNova = HashMD5Service.Encrypt(senhaNova);
                    var usuario = _usuarioRepository.ObtemPorId(usuarioId);
                    if (validaAlteracaoSenha(usuario, senhaAntiga, senhaNova))
                    {
                        _usuarioRepository.AlterarSenha(usuario, senhaNova);
                        dataContext.Commit();
                        ValidationResponse.SetMessageAndType("Senha alterada com sucesso.", ResponseTypeEnum.Success, (int) StatusCodeEnum.Success);
                    }
                }
                catch
                {
                    dataContext.Rollback();
                    ValidationResponse.SetMessageAndType("Erro ao alterar senha.", ResponseTypeEnum.Error, (int) StatusCodeEnum.Error);
                }
                finally
                {
                    dataContext.Finally();
                }
            }
        }

        private static bool validaAlteracaoSenha(Usuario usuario, string senhaAntiga, string senhaNova)
        {
            ValidationResponse.SetDefaultValues();
            if (usuario.UsuarioId == 0)
            {
                ValidationResponse.AddValidationError("usuario", "Usuário inválido.");
            }
            if (usuario.Senha != senhaAntiga)
            {
                ValidationResponse.AddValidationError("senha_antiga", "Senha antiga inválida.");
            }
            if (string.IsNullOrEmpty(senhaNova) || senhaAntiga == senhaNova)
            {
                ValidationResponse.AddValidationError("senha_nova", "Não houve alteração de senha.");
            }
            if (ValidationResponse.ValidationErrors.Count > 0)
            {
                ValidationResponse.SetMessageAndType("Informe os dados corretamente.", ResponseTypeEnum.Warning, (int) StatusCodeEnum.Warning);
            }
            return ValidationResponse.Type != ResponseTypeEnum.Warning;
        }

        private static bool ValidaExclusao(int usuarioId)
        {
            ValidationResponse.SetDefaultValues();
            var mensagem = string.Empty;
            if (_apontamentoRepository.ExisteApontamentoPorUsuario(usuarioId))
            {
                ValidationResponse.AddValidationError("tarefa", "Existe apontamento com o usuário.");
                mensagem = "Existe apontamento com o usuário.";
            }
            else if (_tarefaRepository.ExisteTarefaPorUsuario(usuarioId))
            {
                ValidationResponse.AddValidationError("tarefa", "Existe tarefas para o usuário.");
                mensagem = "Existe tarefas para o usuário.";
            }
            if (ValidationResponse.ValidationErrors.Count > 0)
            {
                ValidationResponse.SetMessageAndType("Não foi possível realizar a exclusão! " + mensagem, ResponseTypeEnum.Warning, (int) StatusCodeEnum.Warning);
            }
            return ValidationResponse.Type != ResponseTypeEnum.Warning;
        }

        private static bool ValidaUsuario(Usuario usuario)
        {
            ValidationResponse.SetDefaultValues();
            if (string.IsNullOrEmpty(usuario.NomeCompleto))
            {
                ValidationResponse.AddValidationError("nome_completo", "Informe o nome completo do colaborador.");
            }
            // if (usuario.Salario == 0)
            // {
            //     ValidationResponse.AddValidationError("salario", "Informe o salário do colaborador.");
            // }
            if (usuario.ValorHora == 0 && usuario.Gerente == false)
            {
                ValidationResponse.AddValidationError("valor_hora", "Informe o valor hora do colaborador.");
            }
            if (string.IsNullOrEmpty(usuario.NomeUsuario))
            {
                ValidationResponse.AddValidationError("nome_usuario", "Informe o nome de usuário do colaborador.");
            }
            if (string.IsNullOrEmpty(usuario.Senha))
            {
                ValidationResponse.AddValidationError("senha", "Informe a senha de acesso.");
            }
            if (ValidationResponse.ValidationErrors.Count > 0)
            {
                ValidationResponse.SetMessageAndType("Informe os dados do colaborador corretamente.", ResponseTypeEnum.Warning, (int) StatusCodeEnum.Warning);
            }
            return ValidationResponse.Type != ResponseTypeEnum.Warning;
        }
    }
}