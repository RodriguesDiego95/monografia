using System;
using System.Collections.Generic;
using api.enums;
using api.models;
using api.repositories;

namespace api.services
{
    public static class ResultadoService
    {
        private static readonly ResultadoRepository _resultadoRepository = new ResultadoRepository();
        private static ValidationResponse _validationResponse = new ValidationResponse();

        public static ValidationResponse ValidationResponse
        {
            get
            {
                return _validationResponse ?? new ValidationResponse();
            }
        }

        public static List<ResultadoCategoria> GetAllResultadoCategoria(DateTime dataInicial, DateTime dataFinal)
        {
            using(var dataContext = new DataContext())
            {
                try
                {
                    ValidationResponse.SetDefaultValues();
                    dataContext.BeginTransaction();
                    var resultadosCategoria = _resultadoRepository.GetAllResultadoCategoria(dataInicial, dataFinal);
                    ValidationResponse.SetMessageAndType("Resultados de categoria recuperados com sucesso.", ResponseTypeEnum.Success, (int) StatusCodeEnum.Success);
                    return resultadosCategoria;
                }
                catch
                {
                    dataContext.Rollback();
                    ValidationResponse.SetMessageAndType("Erro ao recuperar resultados de categoria.", ResponseTypeEnum.Error, (int) StatusCodeEnum.Error);
                    return new List<ResultadoCategoria>();
                }
                finally
                {
                    dataContext.Finally();
                }
            }
        }

        public static List<ResultadoDesempenho> GetAllResultadoDesempenho(int projetoId)
        {
            using(var dataContext = new DataContext())
            {
                try
                {
                    ValidationResponse.SetDefaultValues();
                    dataContext.BeginTransaction();
                    var resultadosDesempenho = _resultadoRepository.GetAllResultadoDesempenho(projetoId);
                    ValidationResponse.SetMessageAndType("Resultados de desempenho recuperados com sucesso.", ResponseTypeEnum.Success, (int) StatusCodeEnum.Success);
                    return resultadosDesempenho;
                }
                catch
                {
                    dataContext.Rollback();
                    ValidationResponse.SetMessageAndType("Erro ao recuperar resultados de desempenho.", ResponseTypeEnum.Error, (int) StatusCodeEnum.Error);
                    return new List<ResultadoDesempenho>();
                }
                finally
                {
                    dataContext.Finally();
                }
            }
        }

        public static ResultadoProjeto GetAllResultadoProjeto(int projetoId)
        {
            using(var dataContext = new DataContext())
            {
                try
                {
                    ValidationResponse.SetDefaultValues();
                    dataContext.BeginTransaction();
                    var resultadoProjeto = _resultadoRepository.GetAllResultadoProjeto(projetoId);
                    ValidationResponse.SetMessageAndType("Resultado de projeto recuperado com sucesso.", ResponseTypeEnum.Success, (int) StatusCodeEnum.Success);
                    return resultadoProjeto;
                }
                catch
                {
                    dataContext.Rollback();
                    ValidationResponse.SetMessageAndType("Erro ao recuperar resultados de projeto.", ResponseTypeEnum.Error, (int) StatusCodeEnum.Error);
                    return new ResultadoProjeto();
                }
                finally
                {
                    dataContext.Finally();
                }
            }
        }

        public static List<ResultadoTarefa> GetAllResultadoTarefas(int projetoId)
        {
            using(var dataContext = new DataContext())
            {
                try
                {
                    ValidationResponse.SetDefaultValues();
                    dataContext.BeginTransaction();
                    var resultadosTarefas = _resultadoRepository.GetAllResultadoTarefas(projetoId);
                    ValidationResponse.SetMessageAndType("Resultado de tarefas recuperado com sucesso.", ResponseTypeEnum.Success, (int) StatusCodeEnum.Success);
                    return resultadosTarefas;
                }
                catch
                {
                    dataContext.Rollback();
                    ValidationResponse.SetMessageAndType("Erro ao recuperar resultados de tarefas.", ResponseTypeEnum.Error, (int) StatusCodeEnum.Error);
                    return new List<ResultadoTarefa>();
                }
                finally
                {
                    dataContext.Finally();
                }
            }
        }
    }
}