using System;
using System.Collections.Generic;
using api.enums;
using api.models;
using api.repositories;

namespace api.services
{
    public static class ApontamentoService
    {
    private static readonly ApontamentoRepository _apontamentoRepository = new ApontamentoRepository();
    private static readonly TarefaRepository _tarefaRepository = new TarefaRepository();
    private static ValidationResponse _validationResponse = new ValidationResponse();

    public static ValidationResponse ValidationResponse
    {
    get
    {
    return _validationResponse ?? new ValidationResponse();
            }
        }

        public static void Add(Apontamento apontamento)
        {
            using(var dataContext = new DataContext())
            {
                try
                {
                    dataContext.BeginTransaction();
                    if (ValidaApontamento(apontamento))
                    {
                        if (apontamento.ApontamentoId > 0)
                        {
                            _apontamentoRepository.Update(apontamento);
                        }
                        else
                        {
                            _apontamentoRepository.Add(apontamento);
                        }
                        dataContext.Commit();
                        ValidationResponse.SetMessageAndType("Apontamento cadastrado com sucesso.", ResponseTypeEnum.Success, (int) StatusCodeEnum.Success);
                    }
                }
                catch
                {
                    dataContext.Rollback();
                    ValidationResponse.SetMessageAndType("Erro ao cadastrar apontamento.", ResponseTypeEnum.Error, (int) StatusCodeEnum.Error);
                }
                finally
                {
                    dataContext.Finally();
                }
            }
        }

        public static List<Apontamento> GetAll(int usuarioId)
        {
            using(var dataContext = new DataContext())
            {
                try
                {
                    dataContext.BeginTransaction();
                    var apontamentos = _apontamentoRepository.GetAll(usuarioId);
                    ValidationResponse.SetMessageAndType("Apontamentos recuperados com sucesso.", ResponseTypeEnum.Success, (int) StatusCodeEnum.Success);
                    return apontamentos;
                }
                catch
                {
                    dataContext.Rollback();
                    ValidationResponse.SetMessageAndType("Erro ao recuperar apontamentos.", ResponseTypeEnum.Error, (int) StatusCodeEnum.Error);
                    return new List<Apontamento>();
                }
                finally
                {
                    dataContext.Finally();
                }
            }
        }

        public static List<Apontamento> ObtemPorFiltro(int usuarioId, DateTime dataInicial, DateTime dataFinal) 
        {
            using(var dataContext = new DataContext())
            {
                try
                {
                    dataContext.BeginTransaction();
                    var apontamentos = _apontamentoRepository.ObtemPorFiltro(usuarioId, dataInicial, dataFinal);
                    ValidationResponse.SetMessageAndType("Apontamentos recuperados com sucesso.", ResponseTypeEnum.Success, (int) StatusCodeEnum.Success);
                    return apontamentos;
                }
                catch
                {
                    dataContext.Rollback();
                    ValidationResponse.SetMessageAndType("Erro ao recuperar apontamentos.", ResponseTypeEnum.Error, (int) StatusCodeEnum.Error);
                    return new List<Apontamento>();
                }
                finally
                {
                    dataContext.Finally();
                }
            }
        }

        public static void Delete(int apontamentoId)
        {
            using(var dataContext = new DataContext())
            {
                try
                {
                    dataContext.BeginTransaction();
                    ValidationResponse.SetDefaultValues();
                    if (apontamentoId == 0)
                    {
                        ValidationResponse.SetMessageAndType("Apontamento não informado para a exclusão.", ResponseTypeEnum.Warning, (int) StatusCodeEnum.Warning);
                    }
                    else
                    {
                        _apontamentoRepository.Delete(apontamentoId);
                        dataContext.Commit();
                        ValidationResponse.SetMessageAndType("Apontamento excluído com sucesso.", ResponseTypeEnum.Success, (int) StatusCodeEnum.Success);
                    }
                }
                catch
                {
                    dataContext.Rollback();
                    ValidationResponse.SetMessageAndType("Erro ao excluir apontamento.", ResponseTypeEnum.Error, (int) StatusCodeEnum.Error);
                }
                finally
                {
                    dataContext.Finally();
                }
            }
        }

        private static bool ValidaApontamento(Apontamento apontamento)
        {
            ValidationResponse.SetDefaultValues();
            if (string.IsNullOrEmpty(apontamento.Descricao))
            {
                ValidationResponse.AddValidationError("descricao", "Informe a descrição do apontamento.");
            }
            if (apontamento.Categoria < 1 || apontamento.Categoria > 3)
            {
                ValidationResponse.AddValidationError("categoria", "Informe a categoria do apontamento.");
            }
            if (apontamento.Data == new DateTime())
            {
                ValidationResponse.AddValidationError("data", "Informe a data do apontamento.");
            }
            if (apontamento.HoraInicial > apontamento.HoraFinal)
            {
                ValidationResponse.AddValidationError("hora_inicial", "Informe a hora inicial corretamente.");
            }
            if (apontamento.HoraFinal < apontamento.HoraInicial)
            {
                ValidationResponse.AddValidationError("hora_final", "Informe a hora final corretamente.");
            }
            if (apontamento.HoraInicial == apontamento.HoraFinal)
            {
                ValidationResponse.AddValidationError("hora_final", "A hora final deve ser maior que a hora inicial.");
            }
            if (apontamento.Tarefa.TarefaId > 0 && _apontamentoRepository.ExisteApontamentoTarefaFinalizada(apontamento.Tarefa.TarefaId,apontamento.ApontamentoId))
            {
                ValidationResponse.AddValidationError("selectTarefa", "Essa tarefa já foi finalizada.");
            }
            if (ValidationResponse.ValidationErrors.Count > 0)
            {
                ValidationResponse.SetMessageAndType("Informe os dados do projeto corretamente.", ResponseTypeEnum.Warning, (int) StatusCodeEnum.Warning);
            }
            return ValidationResponse.Type != ResponseTypeEnum.Warning;
        }
    }
}