using System;
using System.Collections.Generic;
using api.enums;
using api.models;
using api.repositories;

namespace api.services
{
    public static class ProjetoService
    {
    private static readonly ProjetoRepository _projetoRepository = new ProjetoRepository();
    private static readonly TarefaRepository _tarefaRepository = new TarefaRepository();
    private static ValidationResponse _validationResponse = new ValidationResponse();

    public static ValidationResponse ValidationResponse
    {
    get
    {
    return _validationResponse ?? new ValidationResponse();
            }
        }

        public static void Add(Projeto projeto)
        {
            using(var dataContext = new DataContext())
            {
                try
                {
                    dataContext.BeginTransaction();
                    if (ValidaProjeto(projeto))
                    {
                        if (projeto.ProjetoId > 0)
                        {
                            _projetoRepository.Update(projeto);
                        }
                        else
                        {
                            _projetoRepository.Add(projeto);
                        }
                        dataContext.Commit();
                        ValidationResponse.SetMessageAndType("Projeto salvo com sucesso.", ResponseTypeEnum.Success, (int) StatusCodeEnum.Success);
                    }
                }
                catch
                {
                    dataContext.Rollback();
                    ValidationResponse.SetMessageAndType("Erro ao salvar projeto.", ResponseTypeEnum.Error, (int) StatusCodeEnum.Error);
                }
                finally
                {
                    dataContext.Finally();
                }
            }
        }

        public static List<Projeto> GetAll()
        {
            using(var dataContext = new DataContext())
            {
                try
                {
                    dataContext.BeginTransaction();
                    var projetos = _projetoRepository.GetAll();
                    ValidationResponse.SetMessageAndType("Projetos recuperados com sucesso.", ResponseTypeEnum.Success, (int) StatusCodeEnum.Success);
                    return projetos;
                }
                catch
                {
                    dataContext.Rollback();
                    ValidationResponse.SetMessageAndType("Erro ao recuperar projetos.", ResponseTypeEnum.Error, (int) StatusCodeEnum.Error);
                    return new List<Projeto>();
                }
                finally
                {
                    dataContext.Finally();
                }
            }
        }

        private static bool ValidaProjeto(Projeto projeto)
        {
            ValidationResponse.SetDefaultValues();
            if (string.IsNullOrEmpty(projeto.Nome))
            {
                ValidationResponse.AddValidationError("nome", "Informe o nome do projeto.");
            }
            if (string.IsNullOrEmpty(projeto.Descricao))
            {
                ValidationResponse.AddValidationError("descricao", "Informe a descrição do projeto.");
            }
            if (ValidationResponse.ValidationErrors.Count > 0)
            {
                ValidationResponse.SetMessageAndType("Informe os dados do projeto corretamente.", ResponseTypeEnum.Warning, (int) StatusCodeEnum.Warning);
            }
            return ValidationResponse.Type != ResponseTypeEnum.Warning;
        }

        public static void Delete(int projetoId)
        {
            using(var dataContext = new DataContext())
            {
                try
                {
                    dataContext.BeginTransaction();
                    if (ValidaExclusao(projetoId))
                    {
                        _projetoRepository.Delete(projetoId);
                        dataContext.Commit();
                        ValidationResponse.SetMessageAndType("Projeto excluído com sucesso.", ResponseTypeEnum.Success, (int) StatusCodeEnum.Success);
                    }
                }
                catch
                {
                    dataContext.Rollback();
                    ValidationResponse.SetMessageAndType("Erro ao excluir projeto.", ResponseTypeEnum.Error, (int) StatusCodeEnum.Error);
                }
                finally
                {
                    dataContext.Finally();
                }
            }
        }

        private static bool ValidaExclusao(int projetoId)
        {
            ValidationResponse.SetDefaultValues();
            var projeto = _projetoRepository.Get(projetoId);
            var mensagem = string.Empty;
            if (projeto.ProjetoId == 0)
            {
                ValidationResponse.AddValidationError("projeto_id", "Projeto não encontrado.");
                mensagem = "Projeto não encontrado.";
            }
            else if (_tarefaRepository.ExisteTarefa(projetoId))
            {
                ValidationResponse.AddValidationError("tarefa", "Existe tarefa no projeto.");
                mensagem = "Existe tarefa no projeto.";
            }
            if (ValidationResponse.ValidationErrors.Count > 0)
            {
                ValidationResponse.SetMessageAndType("Não foi possível realizar a exclusão! " + mensagem, ResponseTypeEnum.Warning, (int) StatusCodeEnum.Warning);
            }
            return ValidationResponse.Type != ResponseTypeEnum.Warning;
        }
    }
}