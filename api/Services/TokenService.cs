using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using api.models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace api.services
{
    public class TokenService
    {
        private readonly IConfiguration _configuration;

        public TokenService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string GenerateSignToken(Usuario usuario)
        {
            return GenerateToken(usuario.UsuarioId, 86400);
        }

        public string GenerateEmailConfirmationToken(Usuario usuario)
        {
            return GenerateToken(usuario.UsuarioId, 86400);
        }

        public int GetIdToken(ClaimsPrincipal user)
        {
            return Convert.ToInt32(user.Claims.FirstOrDefault(c => c.Type == "id").Value);
        }

        private string GenerateToken(int id, int seconds)
        {
            var claims = new []
            {
                new Claim("id", id.ToString()),
                new Claim(ClaimTypes.Name, id.ToString()),
                new Claim(JwtRegisteredClaimNames.Sub, id.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["TokenConfigurations:SigningKey"]));
            var signingCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer : _configuration["TokenConfigurations:Issuer"],
                audience : _configuration["TokenConfigurations:Audience"],
                claims : claims,
                expires : DateTime.UtcNow.AddSeconds(seconds),
                notBefore : DateTime.UtcNow,
                signingCredentials : signingCredentials
            );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}