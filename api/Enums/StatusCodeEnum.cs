namespace api.enums
{
    public enum StatusCodeEnum
    {
        None = 0, // Null
        Success = 201,
        Warning = 422, // False
        Error = 400 // Exception
    }
}