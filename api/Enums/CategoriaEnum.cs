namespace api.enums
{
    public enum CategoriaEnum
    {
        None = 0, // Null
        Produtivo = 1, // Tempo produtivo
        Investimento = 2, // Investimentos
        Perda = 3 // Perdas gerenciais
    }
}