using System;

namespace api.models
{
    public class ResultadoProjeto
    {
        private Projeto _projeto = new Projeto();
        private string _textoTempoGasto = string.Empty;

        public decimal TempoGasto { get; set; }
        public decimal CustoRealizado {get;set;}
        public decimal EstimativaCusto {get;set;}
        public int SomaEstimativaTempo { get; set; }
        public int SomaEstimativaTarefasFinalizadas { get; set; }
        public Projeto Projeto
        {
            get
            {
                return _projeto ?? new Projeto();
            }
            set
            {
                _projeto = value;
            }
        }
        public string TextoTempoGasto
        {
            get
            {
                return _textoTempoGasto ?? "00:00:00";
            }
            set
            {
                _textoTempoGasto = value;
            }
        }

        public decimal PercentualProjetoFinalizado
        {
            get
            {
                decimal perc;
                try
                {
                    perc = (SomaEstimativaTarefasFinalizadas * 100) / SomaEstimativaTempo;
                }
                catch
                {
                    perc = 0M;
                }
                return (decimal) perc;
            }
        }
    }
}