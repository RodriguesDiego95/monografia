using System.Collections.Generic;
using api.enums;
using api.models;

namespace api.models
{
    public class ValidationResponse
    {
        private int _statusCode = 0;
        private string _message = string.Empty;
        private ResponseTypeEnum _type = ResponseTypeEnum.None;
        private List<ValidationError> _validationErrors = new List<ValidationError>();

        public string Message
        {
            get
            {
                return _message ?? string.Empty;
            }
        }

        public int StatusCode
        {
            get
            {
                return _statusCode;
            }
        }

        public ResponseTypeEnum Type
        {
            get
            {
                return _type;
            }
        }

        public List<ValidationError> ValidationErrors
        {
            get
            {
                return _validationErrors ?? new List<ValidationError>();
            }
        }

        public void AddValidationError(string field, string message = "")
        {
            _validationErrors.Add(new ValidationError()
            {
                Field = field,
                    Message = message
            });
        }

        public void SetMessageAndType(string message, ResponseTypeEnum type, int statusCode)
        {
            _message = message;
            _type = type;
            _statusCode = statusCode;
        }

        public void SetDefaultValues()
        {
            _message = string.Empty;
            _type = ResponseTypeEnum.None;
            _validationErrors = new List<ValidationError>();
            _statusCode = 0;
        }
    }
}