using System;

namespace api.models
{
    public class ResultadoDesempenho
    {
        private Usuario _usuario = new Usuario();
        private Projeto _projeto = new Projeto();
        private string _textoTempoGasto = string.Empty;

        public decimal TempoGasto { get; set; }
        public int SomaPrevisaoTempo { get; set; }
        public int SomaPrevisaoTarefasFinalizadas { get; set; }
        public Usuario Usuario
        {
            get
            {
                return _usuario ?? new Usuario();
            }
            set
            {
                _usuario = value;
            }
        }
        public Projeto Projeto
        {
            get
            {
                return _projeto ?? new Projeto();
            }
            set
            {
                _projeto = value;
            }
        }
        public string TextoTempoGasto
        {
            get
            {
                return _textoTempoGasto ?? "00:00:00";
            }
            set
            {
                _textoTempoGasto = value;
            }
        }

        public decimal PercentualProjetoFinalizado
        {
            get
            {
                decimal perc;
                try
                {
                    perc = (SomaPrevisaoTarefasFinalizadas * 100) / SomaPrevisaoTempo;
                }
                catch
                {
                    perc = 0M;
                }
                return (decimal) perc;
            }
        }

        public decimal CustoRealizado
        {
            get
            {
                return TempoGasto * Usuario.ValorHora;
            }
        }

        public decimal CustoPrevisto
        {
            get
            {
                return SomaPrevisaoTempo * Usuario.ValorHora;
            }
        }
    }
}