using System;

namespace api.models
{
    public class ResultadoCategoria
    {
        private Usuario _usuario = new Usuario();
        // private decimal _custoProdutividade = 0M;
        // private decimal _custoInvestimento = 0M;
        // private decimal _custoPerda = 0M;
        public string TextoProdutividade { get; set; }
        public string TextoInvestimento { get; set; }
        public string TextoPerda { get; set; }
        public decimal HorasProdutividade { get; set; }
        public decimal HorasInvestimento { get; set; }
        public decimal HorasPerda { get; set; }
        public Usuario Usuario
        {
            get
            {
                return _usuario ?? new Usuario();
            }
            set
            {
                _usuario = value;
            }
        }
        public decimal CustoProdutividade
        {
            get
            {
                return (decimal) HorasProdutividade * Usuario.ValorHora;
            }
        }
        public decimal CustoInvestimento
        {
            get
            {
                return (decimal) HorasInvestimento * Usuario.ValorHora;
            }
        }
        public decimal CustoPerda
        {
            get
            {
                return (decimal) HorasPerda * Usuario.ValorHora;
            }
        }
        public decimal CustoTotal
        {
            get
            {
                return (decimal) CustoProdutividade + CustoInvestimento + CustoPerda;
            }
        }
        public decimal PercentualProdutividade
        {
            get
            {
                decimal perc;
                try
                {
                    perc = (CustoProdutividade * 100) / CustoTotal;
                }
                catch
                {
                    perc = 0M;
                }
                return (decimal) perc;
            }
        }
        public decimal PercentualInvestimento
        {
            get
            {
                decimal perc;
                try
                {
                    perc = (CustoInvestimento * 100) / CustoTotal;
                }
                catch
                {
                    perc = 0M;
                }
                return (decimal) perc;
            }
        }
        public decimal PercentualPerda
        {
            get
            {
                decimal perc;
                try
                {
                    perc = (CustoPerda * 100) / CustoTotal;
                }
                catch
                {
                    perc = 0M;
                }
                return (decimal) perc;
            }
        }
        // Para somar tempo no mysql:
        // select sec_to_time(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial)))) from apontamento
    }
}