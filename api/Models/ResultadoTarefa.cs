using System;

namespace api.models
{
    public class ResultadoTarefa
    {
        private Usuario _usuario = new Usuario();
        private Tarefa _tarefa = new Tarefa();
        private string _textoTempoGasto = string.Empty;

        public decimal TempoGasto { get; set; }
        public int SomaPrevisaoTempo { get; set; }
        public DateTime PrevisaoFim { get; set; }
        public string DataFinalizacao { get; set; }
        public bool TarefaFinalizada { get; set; }
        public Usuario Usuario
        {
            get
            {
                return _usuario ?? new Usuario();
            }
            set
            {
                _usuario = value;
            }
        }
        public Tarefa Tarefa
        {
            get
            {
                return _tarefa ?? new Tarefa();
            }
            set
            {
                _tarefa = value;
            }
        }
        public string TextoTempoGasto
        {
            get
            {
                return _textoTempoGasto ?? "00:00:00";
            }
            set
            {
                _textoTempoGasto = value;
            }
        }

        public decimal CustoRealizado
        {
            get
            {
                return TempoGasto * Usuario.ValorHora;
            }
        }

        public decimal CustoPrevisto
        {
            get
            {
                return SomaPrevisaoTempo * Usuario.ValorHora;
            }
        }
    }
}