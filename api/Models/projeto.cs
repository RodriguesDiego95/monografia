using System;

namespace api.models
{
    public class Projeto
    {
        private string _nome = string.Empty;
        private string _descricao = string.Empty;

        public int ProjetoId { get; set; }
        public int PrevisaoTempoGasto { get; set; }
        public int TempoGastoReal { get; set; }
        public DateTime PrevisaoInicio { get; set; }
        public DateTime PrevisaoFim { get; set; }
        public DateTime InicioReal { get; set; }
        public DateTime FimReal { get; set; }
        public string Nome
        {
            get
            {
                return _nome ?? string.Empty;
            }
            set
            {
                _nome = value;
            }
        }
        public string Descricao
        {
            get
            {
                return _descricao ?? string.Empty;
            }
            set
            {
                _descricao = value;
            }
        }
    }
}