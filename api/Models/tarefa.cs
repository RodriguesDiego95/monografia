using System;

namespace api.models
{
    public class Tarefa
    {
        private string _descricao = string.Empty;
        private Projeto _projeto = new Projeto();
        private Usuario _usuario = new Usuario();
        public int TarefaId { get; set; }
        public DateTime PrevisaoInicio { get; set; }
        public DateTime PrevisaoFim { get; set; }
        public int PrevisaoTempoGasto { get; set; }
        public string Descricao
        {
            get
            {
                return _descricao ?? string.Empty;
            }
            set
            {
                _descricao = value;
            }
        }
        public Projeto Projeto
        {
            get
            {
                return _projeto ?? new Projeto();
            }
            set
            {
                _projeto = value;
            }
        }
        public Usuario Usuario
        {
            get
            {
                return _usuario ?? new Usuario();
            }
            set
            {
                _usuario = value;
            }
        }
        // Valores para exibição de resultados
        private decimal _valorEstimado = 0M;
        public decimal ValorEstimado
        {
            get
            {
                return Math.Max(0, _valorEstimado);
            }
            set
            {
                _valorEstimado = Math.Max(0, value);
            }
        }
    }
}