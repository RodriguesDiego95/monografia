using System;

namespace api.models
{
    public class Apontamento
    {
        private string _descricao = string.Empty;
        private Usuario _usuario = new Usuario();
        private Projeto _projeto = new Projeto();
        private Tarefa _tarefa = new Tarefa();

        public int ApontamentoId { get; set; }
        public int Categoria { get; set; }
        public DateTime Data { get; set; }
        public TimeSpan HoraInicial { get; set; }
        public TimeSpan HoraFinal { get; set; }
        public bool TarefaFinalizada { get; set; }
        public string Descricao
        {
            get
            {
                return _descricao ?? string.Empty;
            }
            set
            {
                _descricao = value;
            }
        }
        public Usuario Usuario
        {
            get
            {
                return _usuario ?? new Usuario();
            }
            set
            {
                _usuario = value;
            }
        }
        public Projeto Projeto
        {
            get
            {
                return _projeto ?? new Projeto();
            }
            set
            {
                _projeto = value;
            }
        }
        public Tarefa Tarefa
        {
            get
            {
                return _tarefa ?? new Tarefa();
            }
            set
            {
                _tarefa = value;
            }
        }
        public TimeSpan TempoGasto
        {
            get
            {
                return HoraFinal - HoraInicial;
            }
        }
    }
}