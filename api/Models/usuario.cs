using System;

namespace api.models
{
    public class Usuario
    {
        private string _nomeCompleto = string.Empty;
        private string _nomeUsuario = string.Empty;
        private string _senha = string.Empty;
        private decimal _salario = 0M;
        private decimal _valorHora = 0M;

        public int UsuarioId { get; set; }
        public bool Gerente { get; set; }
        public int HorasSemanais { get; set; }
        public decimal Salario
        {
            get
            {
                return Math.Max(0, _salario);
            }
            set
            {
                _salario = Math.Max(0, value);
            }
        }
        public decimal ValorHora
        {
            get
            {
                return Math.Max(0, _valorHora);
            }
            set
            {
                _valorHora = Math.Max(0, value);
            }
        }
        public string NomeCompleto
        {
            get
            {
                return _nomeCompleto ?? string.Empty;
            }
            set
            {
                _nomeCompleto = value;
            }
        }
        public string NomeUsuario
        {
            get
            {
                return _nomeUsuario ?? string.Empty;
            }
            set
            {
                _nomeUsuario = value;
            }
        }
        public string Senha
        {
            get
            {
                return _senha ?? string.Empty;
            }
            set
            {
                _senha = value;
            }
        }
    }
}