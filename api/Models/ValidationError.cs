namespace api.models
{
    public class ValidationError
    {
        private string _field = string.Empty;
        private string _message = string.Empty;

        public string Field
        {
            get
            {
                return _field ?? string.Empty;
            }
            set
            {
                _field = value;
            }
        }

        public string Message
        {
            get
            {
                return _message ?? string.Empty;
            }
            set
            {
                _message = value;
            }
        }
    }
}