DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `nome_completo` varchar(250) NOT NULL DEFAULT '',
  `salario` decimal(15,2) NOT NULL DEFAULT '0.00',
  `valor_hora` decimal(15,2) NOT NULL DEFAULT '0.00',
  `gerente` tinyint(1) NOT NULL DEFAULT '0',
  `nome_usuario` varchar(20) NOT NULL,
  `senha` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `projeto`;
CREATE TABLE `projeto` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) NOT NULL,
  `descricao` text NOT NULL,
  `previsao_inicio` datetime DEFAULT NULL,
  `previsao_fim` datetime DEFAULT NULL,
  `previsao_tempo_gasto` int(15) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `tarefa`;
CREATE TABLE `tarefa` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_projeto` int(10) NOT NULL,
  `id_usuario` int(10) NOT NULL,
  `descricao` text NOT NULL,
  `previsao_inicio` datetime DEFAULT '0000-00-00 00:00:00',
  `previsao_tempo_gasto` int(15) DEFAULT '0',
  `previsao_fim` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `projeto_tarefas_projeto` (`id_projeto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `apontamento`;
CREATE TABLE `apontamento` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(10) NOT NULL,
  `id_tarefa` int(10) NOT NULL DEFAULT '0',
  `descricao` text NOT NULL,
  `data` datetime NOT NULL,
  `hora_inicial` time NOT NULL,
  `hora_final` time NOT NULL,
  `categoria` int(1) NOT NULL,
  `excluido` int(1) NOT NULL DEFAULT '0',
  `tarefa_finalizada` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `apontamentos_usuario` (`id_usuario`),
  KEY `apontamentos_tarefa` (`id_tarefa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;