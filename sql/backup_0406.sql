/*
Navicat MySQL Data Transfer

Source Server         : novopdv
Source Server Version : 50148
Source Host           : localhost:3306
Source Database       : ___monografia

Target Server Type    : MYSQL
Target Server Version : 50148
File Encoding         : 65001

Date: 2018-06-04 17:51:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `apontamento`
-- ----------------------------
DROP TABLE IF EXISTS `apontamento`;
CREATE TABLE `apontamento` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(10) NOT NULL,
  `id_tarefa` int(10) NOT NULL DEFAULT '0',
  `descricao` text NOT NULL,
  `data` datetime NOT NULL,
  `hora_inicial` time NOT NULL,
  `hora_final` time NOT NULL,
  `categoria` int(1) NOT NULL,
  `excluido` int(1) NOT NULL DEFAULT '0',
  `tarefa_finalizada` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `apontamentos_usuario` (`id_usuario`),
  KEY `apontamentos_tarefa` (`id_tarefa`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of apontamento
-- ----------------------------
INSERT INTO `apontamento` VALUES ('1', '4', '1', 'Inserir cartao fidelidade no cadastro de clientes.', '2018-05-30 00:00:00', '08:00:00', '09:00:00', '1', '0', '0');
INSERT INTO `apontamento` VALUES ('2', '4', '0', 'Ajustes no migrador da empresa Original Fit', '2018-05-30 00:00:00', '09:00:00', '11:30:00', '1', '0', '0');
INSERT INTO `apontamento` VALUES ('3', '4', '0', 'Ajustes no migrador da empresa Original Fit', '2018-05-30 00:00:00', '12:45:00', '13:30:00', '1', '0', '0');
INSERT INTO `apontamento` VALUES ('4', '4', '1', 'Inserir cartao fidelidade no cadastro de clientes.', '2018-05-30 00:00:00', '13:30:00', '16:00:00', '1', '0', '1');
INSERT INTO `apontamento` VALUES ('5', '4', '0', 'Auxiliar equipe do treinamento com migracao de dados para a empresa Original Fit ', '2018-05-30 00:00:00', '16:00:00', '17:30:00', '1', '0', '0');
INSERT INTO `apontamento` VALUES ('6', '3', '6', 'Pesquisa no Projeto ', '2018-05-30 00:00:00', '13:30:00', '15:00:00', '2', '0', '0');
INSERT INTO `apontamento` VALUES ('7', '3', '6', 'Personalizacao de cores de botoes e labels', '2018-05-30 00:00:00', '15:00:00', '18:00:00', '1', '0', '0');
INSERT INTO `apontamento` VALUES ('8', '4', '0', 'Auxiliar e instruir projeto SommusMelhores', '2018-05-30 00:00:00', '17:30:00', '17:50:00', '1', '0', '0');
INSERT INTO `apontamento` VALUES ('9', '4', '0', 'Apresentar o projeto SommusMelhores ao Bruno', '2018-05-30 00:00:00', '17:50:00', '18:00:00', '3', '0', '0');
INSERT INTO `apontamento` VALUES ('10', '4', '0', 'Alongamentos e oracao', '2018-06-01 00:00:00', '08:00:00', '08:15:00', '3', '0', '0');
INSERT INTO `apontamento` VALUES ('11', '4', '2', 'Alterar exportacao de pessoas para enviar cartao fidelidade', '2018-06-01 00:00:00', '08:15:00', '10:20:00', '1', '0', '0');
INSERT INTO `apontamento` VALUES ('12', '4', '0', 'Ajustar cadastro de grade no migrador da empresa Original Fit', '2018-06-01 00:00:00', '10:20:00', '11:30:00', '1', '0', '0');
INSERT INTO `apontamento` VALUES ('13', '4', '2', 'Alterar exportacao de pessoas para enviar cartao fidelidade', '2018-06-01 00:00:00', '11:45:00', '14:00:00', '1', '0', '0');
INSERT INTO `apontamento` VALUES ('14', '4', '0', '[SommusMelhores] Instruir e auxiliar no projeto', '2018-06-01 00:00:00', '14:00:00', '18:00:00', '1', '0', '0');
INSERT INTO `apontamento` VALUES ('15', '3', '6', 'Finalizando Personalizacao e fazendo ajustes', '2018-06-01 00:00:00', '13:30:00', '16:30:00', '1', '0', '0');
INSERT INTO `apontamento` VALUES ('16', '4', '2', 'Desenvolver identificador de cliente por cartao fidelidade', '2018-06-04 00:00:00', '08:00:00', '08:30:00', '1', '0', '0');
INSERT INTO `apontamento` VALUES ('17', '3', '6', 'Testes e Ajustes', '2018-06-01 00:00:00', '16:30:00', '18:00:00', '1', '0', '0');
INSERT INTO `apontamento` VALUES ('18', '3', '6', 'Fazendo Ajustes', '2018-06-04 00:00:00', '13:30:00', '14:00:00', '1', '0', '0');
INSERT INTO `apontamento` VALUES ('19', '4', '0', 'Auxiliar Wilson a respeito de backup das migracoes de dados', '2018-06-04 00:00:00', '08:30:00', '09:10:00', '1', '0', '0');
INSERT INTO `apontamento` VALUES ('20', '3', '6', 'Reuniao de demonstracao do app', '2018-06-04 00:00:00', '14:00:00', '15:50:00', '2', '0', '0');
INSERT INTO `apontamento` VALUES ('21', '4', '0', 'Ajustes no migrador da empresa Original Fit', '2018-06-04 00:00:00', '09:10:00', '11:30:00', '1', '0', '0');
INSERT INTO `apontamento` VALUES ('22', '3', '0', 'Preenchendo Apontamentos', '2018-06-04 00:00:00', '15:50:00', '16:00:00', '3', '0', '0');
INSERT INTO `apontamento` VALUES ('23', '4', '0', 'Ajustes no migrador da empresa Original Fit', '2018-06-04 00:00:00', '12:45:00', '13:30:00', '1', '0', '0');
INSERT INTO `apontamento` VALUES ('24', '4', '0', '[SommusMelhores] Instruir e auxiliar no projeto', '2018-06-04 00:00:00', '13:30:00', '14:00:00', '1', '0', '0');
INSERT INTO `apontamento` VALUES ('25', '4', '0', 'Reuniao com Reginaldo para decisoes no projeto SommusMelhores', '2018-06-04 00:00:00', '14:00:00', '16:00:00', '2', '0', '0');

-- ----------------------------
-- Table structure for `projeto`
-- ----------------------------
DROP TABLE IF EXISTS `projeto`;
CREATE TABLE `projeto` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) NOT NULL,
  `descricao` text NOT NULL,
  `previsao_inicio` datetime DEFAULT NULL,
  `previsao_fim` datetime DEFAULT NULL,
  `previsao_tempo_gasto` int(15) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of projeto
-- ----------------------------
INSERT INTO `projeto` VALUES ('1', 'App Pesquisa Popular', 'Desenvolvimento de aplicativo (pwa) para pesquisa popular.', '0001-01-01 00:00:00', '0001-01-01 00:00:00', '0');
INSERT INTO `projeto` VALUES ('2', 'Cartao Fidelidade', 'Implementacao de cartao fidelidade', '0001-01-01 00:00:00', '0001-01-01 00:00:00', '0');
INSERT INTO `projeto` VALUES ('3', 'SommusDash', 'Desenvolvimento de sincronizador para integracao com aplicativo SommusDash', '0001-01-01 00:00:00', '0001-01-01 00:00:00', '0');

-- ----------------------------
-- Table structure for `tarefa`
-- ----------------------------
DROP TABLE IF EXISTS `tarefa`;
CREATE TABLE `tarefa` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_projeto` int(10) NOT NULL,
  `id_usuario` int(10) NOT NULL,
  `descricao` text NOT NULL,
  `previsao_inicio` datetime DEFAULT '0000-00-00 00:00:00',
  `previsao_tempo_gasto` int(15) DEFAULT '0',
  `previsao_fim` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `projeto_tarefas_projeto` (`id_projeto`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tarefa
-- ----------------------------
INSERT INTO `tarefa` VALUES ('1', '2', '4', 'Inserir campo para vincular o cartao fidelidade ao cadastro de clientes e conveniados.', '2018-05-30 00:00:00', '5', '2018-05-30 00:00:00');
INSERT INTO `tarefa` VALUES ('2', '2', '4', 'Desenvolver exportacao do cartao fidelidade para o SommusPDV', '2018-05-30 00:00:00', '7', '2018-05-31 00:00:00');
INSERT INTO `tarefa` VALUES ('3', '2', '4', 'Identificar cliente pelo cartao fidelidade no Cupom Fiscal e NFCe', '2018-06-04 00:00:00', '25', '2018-06-06 00:00:00');
INSERT INTO `tarefa` VALUES ('4', '2', '4', 'Identificar cliente pelo cartao fidelidade em todos os modulos de DAV\'s', '2018-06-06 00:00:00', '30', '2018-06-11 00:00:00');
INSERT INTO `tarefa` VALUES ('5', '1', '3', 'Desenvolver cadastro de empresas ', '2018-04-01 00:00:00', '50', '2018-04-05 00:00:00');
INSERT INTO `tarefa` VALUES ('6', '1', '3', 'Desenvolver personalizacao de tela de acordo com a configuracao da instituicao', '2018-05-24 00:00:00', '50', '2018-06-05 00:00:00');
INSERT INTO `tarefa` VALUES ('7', '3', '2', 'Analise de funcionamento inicial do projeto', '2018-05-30 00:00:00', '8', '2018-05-30 00:00:00');
INSERT INTO `tarefa` VALUES ('8', '3', '2', 'Desenvolvimento da primeira consulta de query e gravacao no banco de dados', '2018-06-01 00:00:00', '8', '2018-06-01 00:00:00');

-- ----------------------------
-- Table structure for `usuario`
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `nome_completo` varchar(250) NOT NULL DEFAULT '',
  `salario` decimal(15,2) NOT NULL DEFAULT '0.00',
  `valor_hora` decimal(15,2) NOT NULL DEFAULT '0.00',
  `gerente` tinyint(1) NOT NULL DEFAULT '0',
  `nome_usuario` varchar(20) NOT NULL,
  `senha` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES ('1', 'Aurélio Paim', '0.00', '0.00', '1', 'aurelio', '202cb962ac59075b964b07152d234b70');
INSERT INTO `usuario` VALUES ('2', 'Joao Gabriel', '0.00', '10.00', '0', 'joaogabriel', '202cb962ac59075b964b07152d234b70');
INSERT INTO `usuario` VALUES ('3', 'Bruno', '0.00', '10.00', '0', 'bruno', '202cb962ac59075b964b07152d234b70');
INSERT INTO `usuario` VALUES ('4', 'Diego', '0.00', '10.00', '0', 'diego', '202cb962ac59075b964b07152d234b70');
