-- Resultado Categorias

select 
	id_usuario AS cod_usuario, usuario.nome_completo, usuario.valor_hora,
	(
	 select COALESCE(sec_to_time(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial)))),'00:00:00') 
			from apontamento where id_usuario = cod_usuario and data >= '2018-04-16' and data<='2018-04-16' and categoria=1 
	) AS tempo_produtividade,
	(
	 select COALESCE(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial))),0) / 3600
			from apontamento where id_usuario = cod_usuario and data >= '2018-04-16' and data<='2018-04-16' and categoria=1 
	) AS horas_produtividade,
	(
	 select COALESCE(sec_to_time(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial)))),'00:00:00') 
			from apontamento where id_usuario = cod_usuario and data>='2018-04-16' and data<='2018-04-16' and categoria=2
	) AS tempo_investimento,
	(
	 select COALESCE(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial))),0) / 3600
			from apontamento where id_usuario = cod_usuario and data >= '2018-04-16' and data<='2018-04-16' and categoria=2
	) AS horas_investimento,
	(
	 select COALESCE(sec_to_time(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial)))),'00:00:00') 
			from apontamento where id_usuario = cod_usuario and categoria=3 and data >= '2018-04-16' and data<='2018-04-16'
	) AS tempo_perda,
	(
	 select COALESCE(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial))),0) / 3600
			from apontamento where id_usuario = cod_usuario and data >= '2018-04-16' and data<='2018-04-16' and categoria=3
	) AS horas_perda
from apontamento 
left join usuario on usuario.id = apontamento.id_usuario
group by id_usuario

-- Resultado Tarefas

SELECT 
	usuario.nome_completo AS colaborador, usuario.valor_hora, apontamento.descricao AS apontamento, apontamento.id_tarefa, tarefa.descricao as tarefa,
	apontamento.categoria, tarefa.previsao_tempo_gasto, tarefa.previsao_fim,
	COALESCE(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial))),0) / 3600 as tempo_gasto,
	SEC_TO_TIME(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial)))) as tempo_tarefa,
	(
	SELECT tarefa_finalizada FROM apontamento a WHERE a.id_tarefa = tarefa.id ORDER BY tarefa_finalizada DESC LIMIT 1
	) AS tarefa_finalizada
FROM usuario
LEFT JOIN apontamento ON apontamento.id_usuario = usuario.id 
LEFT JOIN tarefa ON apontamento.id_tarefa = tarefa.id
WHERE id_tarefa > 0
group by colaborador,id_tarefa
order by tarefa.previsao_fim

-- Resultado Tarefas (atualizado)

SELECT 
	tarefa.id, tarefa.descricao as tarefa, usuario.nome_completo AS colaborador, usuario.valor_hora, apontamento.descricao AS apontamento, 
	apontamento.categoria, tarefa.previsao_tempo_gasto, tarefa.previsao_fim,
	COALESCE(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial))),0) / 3600 as tempo_gasto,
	SEC_TO_TIME(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial)))) as tempo_tarefa,
	(
	SELECT tarefa_finalizada FROM apontamento a WHERE a.id_tarefa = tarefa.id ORDER BY tarefa_finalizada DESC LIMIT 1
	) AS tarefa_finalizada
FROM tarefa
LEFT JOIN apontamento ON apontamento.id_tarefa = tarefa.id 
LEFT JOIN usuario ON apontamento.id_usuario = usuario.id
WHERE tarefa.id_projeto = 1
group by colaborador,id_tarefa
order by tarefa.previsao_fim

-- Resultado Desempenho

SELECT 

	usuario.nome_completo AS colaborador, usuario.valor_hora,
	COALESCE(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial))),0) / 3600 as tempo_gasto,
	SEC_TO_TIME(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial)))) as texto_tempo,
	(
		SELECT sum(previsao_tempo_gasto) FROM tarefa t WHERE t.id_usuario = usuario.id AND t.id_projeto = 2
	) AS soma_previsao_tempo_gasto,
	(
		SELECT COALESCE(sum(previsao_tempo_gasto),0) FROM tarefa t LEFT JOIN apontamento a ON t.id = a.id_tarefa AND a.tarefa_finalizada=1 WHERE t.id_projeto=2 AND a.id_usuario=usuario.id
	) AS soma_previsao_tarefas_finalizadas
	
FROM usuario
LEFT JOIN apontamento ON apontamento.id_usuario = usuario.id 
LEFT JOIN tarefa ON apontamento.id_tarefa = tarefa.id
LEFT JOIN projeto ON projeto.id = tarefa.id_projeto
WHERE projeto.id = 2
group by colaborador
order by tarefa.previsao_fim

-- Resultado Projeto 

SELECT 

	COALESCE(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial))),0) / 3600 as tempo_gasto,
	SEC_TO_TIME(sum(time_to_sec(TIMEDIFF(hora_final,hora_inicial)))) as texto_tempo,
	( 
		SELECT SUM(COALESCE((t.previsao_tempo_gasto * u.valor_hora),0)) FROM tarefa t 
		LEFT JOIN usuario u ON t.id_usuario=u.id WHERE id_projeto = 1
	) AS estimativa_custo

FROM apontamento
LEFT JOIN tarefa ON tarefa.id = apontamento.id_tarefa
LEFT JOIN usuario ON usuario.id = tarefa.id_usuario
WHERE tarefa.id_projeto = 1