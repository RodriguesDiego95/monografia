function verificaAutenticacao() {
    $.ajax({
        type: "GET",
        url: getApi() + "/get/verificaAutenticacao",
        cache: false,
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
        success: function () {
            obtemUsuarioLogado();
            loadHome();
        },
        error: function () {
            renderSreenLogin();
        }
    });
}

function renderSreenLogin() {
    renderHtml("#app", "Login/login.html");
}

function verificaConfirmaAutenticacao(e) {
    if (e.keyCode == 13) {
        autenticaLogin();
    }
}

function autenticaLogin() {
    var dados = {
        Username: removeMascara($("#nome_usuario").val()),
        Password: $("#senha").val()
    };
    $.ajax({
        type: "POST",
        url: getApi() + "/account/token",
        data: JSON.stringify(dados),
        cache: false,
        contentType: 'application/json' ,
        success: function (retorno) {
            localStorage.setItem("jtoken", retorno.accessToken);
            obtemUsuarioLogado();
        },
        error: function (retorno) {
            alert(retorno.responseText);
        }
    });
}

function obtemUsuarioLogado() {
    $.ajax({
        type: "GET",
        url: getApi() + "/get/userAutentication",
        cache: false,
        async: false,
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
        success: function (retorno) {
            localStorage.setItem("usuario", JSON.stringify(retorno.usuario));
            loadHome();
        },
        error: function (retorno) {
            alert(retorno.responseText);
            renderSreenLogin();
        }
    });
}

function logout() {
    $(".side-nav").sideNav("hide");
    localStorage.clear();
    verificaAutenticacao();
}