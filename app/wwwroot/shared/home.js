function loadHome() {
    renderHtml("#app", "/Shared/sidebar.html","configuraSideNav();");
}

function configuraSideNav() {
    $(".button-collapse").sideNav();
    var usuario = JSON.parse(localStorage.getItem("usuario"));
    $("#nomeUsuario-sidenav").text(usuario.nomeCompleto);
    $('.collapsible').collapsible();
    configuraDisplayPorUsuario(usuario);
}

function configuraDisplayPorUsuario(usuario) {
    if (usuario.gerente) {
        $(".hidden-admin").hide();
        $(".hidden-dev").show();
        projetoIndex();
    }
    else {
        $(".hidden-dev").hide();
        $(".hidden-admin").show();
        apontamentoIndex();
    }
}
