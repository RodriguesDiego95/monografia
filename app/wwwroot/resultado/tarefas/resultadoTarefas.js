function resultadoTarefasIndex() {
    renderHtml("#conteudo", "/Resultado/Tarefas/index.html", "configuraComponentesResultadoTarefas();");
}

function configuraComponentesResultadoTarefas() {
    $("#titulo-nav").text("Resultados de Tarefas");
    $(".side-nav").sideNav("hide");
    obtemProjetosResultado();
    configuraCollapsible();
}

function aplicarFiltroResultadoTarefas() {
    loadingOpen();

    $.ajax({
        type: "GET",
        url: getApi() + "/get/resultadotarefas?projetoid=" + $("#selectProjeto").val(),
        cache: false,
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
        success: function (retorno) {
            preparaTableResultadoTarefas(retorno.resultadoTarefas);
        },
        error: function (response) {
            alert(response.responseText);
        },
        complete: function () {
            loadingClose();
        }
    });
}

function preparaTableResultadoTarefas(resultadoTarefas) {
    var html = "";
    if (resultadoTarefas.length > 0) {
        $("#conteudo-resultadoTarefas").empty();
        $("#conteudo-resultadoTarefas").append(`
            <div class='card'>
                <div class='card-content'>
                    <table id="tableResultadoTarefas" class="highlight">
                        <thead>
                            <tr>
                                <th class="col s4">Tarefa</th>
                                <th class="col s1 center">Previsão Tempo</th>
                                <th class="col s2 center">Previsão Término</th>
                                <th class="col s2 center">Data Término</th>
                                <th class="col s1 center">Tempo Gasto</th>
                                <th class="col s2">Responsável</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>`
        );
        for (var i = 0; i < resultadoTarefas.length; i++) {
            $("#tableResultadoTarefas > tbody").append(
                "<tr>" +
                "   <td class='col s4'>" + resultadoTarefas[i].tarefa.descricao + "</td>" +
                "   <td class='col s1 center'>" + resultadoTarefas[i].somaPrevisaoTempo+":00:00" + "</td>" +
                "   <td class='col s2 center'>" + formatDate(resultadoTarefas[i].previsaoFim) + "</td>" +
                "   <td class='col s2 center'>" + resultadoTarefas[i].dataFinalizacao.substring(0,10) + "</td>" +
                "   <td class='col s1 center'>" + resultadoTarefas[i].textoTempoGasto + "</td>" +
                "   <td class='col s2'>" + resultadoTarefas[i].usuario.nomeCompleto + "</td>" +
                "</tr>"
            );
        }
    }
    if (resultadoTarefas.length == 0) {
        html += "<div class='card'>"
        html += "   <div class='card-content'>"
        html += "       <span class='card-title center'>Não possui dados a serem exibidos!</span>"
        html += "   </div>"
        html += "</div>"
        $("#conteudo-resultadoTarefas").empty();
        $("#conteudo-resultadoTarefas").append(html)
    }
    configuraCollapsible();
}