function resultadoProjetoIndex() {
    renderHtml("#conteudo", "/Resultado/Projeto/index.html", "configuraComponentesResultadoProjeto();");
}

function configuraComponentesResultadoProjeto() {
    $("#titulo-nav").text("Resultados de Projeto");
    $(".side-nav").sideNav("hide");
    obtemProjetosResultado();
    configuraCollapsible();
}

function obtemProjetosResultado() {
    loadingOpen();
    $.ajax({
        type: "GET",
        url: getApi() + "/get/projeto",
        cache: false,
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
        success: function (retorno) {
            preencheSelectProjeto(retorno.projetos)
        },
        error: function (response) {
            alert(response.responseText);
        },
        complete: function () {
            loadingClose();
        }
    });
}

function aplicarFiltroResultadoProjeto() {
    loadingOpen();

    $.ajax({
        type: "GET",
        url: getApi() + "/get/resultadoprojeto?projetoid=" + $("#selectProjeto").val(),
        cache: false,
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
        success: function (retorno) {
            preparaChartResultadoProjeto(retorno.resultadoProjeto);
        },
        error: function (response) {
            alert(response.responseText);
        },
        complete: function () {
            loadingClose();
        }
    });
}

function preparaChartResultadoProjeto(resultadoProjeto) {
    $("#conteudo-resultadoProjeto").empty();
    if (resultadoProjeto.projeto.projetoId == 0) {
        $("#conteudo-resultadoProjeto").append(
            "<div class='card'>" +
            "   <div class='card-content'>" +
            "       <span class='card-title center'>Não há dados a ser exibido!</span>" +
            "   </div>" +
            "</div>"
        );
    } else {
        $("#conteudo-resultadoProjeto").append(
            "<div class='card col s12 m12 l12'>" +
            "   <h5 class='center'>" + resultadoProjeto.projeto.nome + "</h5>" +
            "   <div class='col s12 m12 l5'>" +
            "       <canvas id='chartResultadoProjeto' width='400' height='250'></canvas>" +
            "   </div>" +
            "   <div id='info-resultadoProjeto' class='col s12 m12 l7'>" +
            "   </div>" +
            "</div>"
        );
        buildChartResultadoProjeto(resultadoProjeto);
        infoResultadoProjeto(resultadoProjeto);
    }
}

function buildChartResultadoProjeto(result) {
    var ctx = document.getElementById("chartResultadoProjeto");
    var data = {
        datasets: [{
            label: ['Resultado em Horas'],
            data: [result.somaEstimativaTempo, result.tempoGasto],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)'
            ],
            borderWidth: 1
        }],
        labels: ['Estimativa', 'Realizado']
    }
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

function infoResultadoProjeto(result) {
    var percentualComparacao = (result.tempoGasto * 100) / result.somaEstimativaTempo
    var comentario = "";
    if (result.percentualProjetoFinalizado >= percentualComparacao * 2.50) {
        comentario = "Projeto com custo muito baixo!"
    }
    else if (result.percentualProjetoFinalizado >= percentualComparacao * 1.20) {
        comentario = "Projeto com baixo custo!"
    }
    else if (result.percentualProjetoFinalizado >= percentualComparacao * 1.05) {
        comentario = "Projeto com o custo um pouco abaixo do normal!"
    }
    else if (result.percentualProjetoFinalizado > percentualComparacao * 0.90 && result.percentualProjetoFinalizado < percentualComparacao * 1.05) {
        comentario = "Projeto de acordo com as estimativas de custo!"
    }
    else if (result.percentualProjetoFinalizado >= percentualComparacao * 0.80) {
        comentario = "O custo do projeto está um pouco acima!"
    }
    else if (result.percentualProjetoFinalizado >= percentualComparacao * 0.60) {
        comentario = "Projeto com custos acima do normal!"
    }
    else if (result.percentualProjetoFinalizado > 0) {
        comentario = "Projeto com muito custo excedente!"
    }
    $("#info-resultadoProjeto").append(
        "<div class='row'></div>" +
        "<div class='col s6'>" +
        "   <span><b>Estimativas</b></span>" +
        "   <p><span>Tempo: " + result.somaEstimativaTempo + ":00:00</span></p>" +
        "   <p>Custo: <span class='currency'> " + result.estimativaCusto.toFixed(2) + "</span></p>" +
        "</div>" +
        "<div class='col s6'>" +
        "   <span><b>Realizado</b></span>" +
        "   <p><span>Tempo: " + result.textoTempoGasto + "</span></p>" +
        "   <p>Custo: <span class='currency'>" + result.custoRealizado.toFixed(2) + "</span></p>" +
        "</div>" +
        "<div class='divider'></div>" +
        "<div class='row'></div>" +
        "<div class='section'>" +
        "   <h5>Percentual de conclusão: <b>" + result.percentualProjetoFinalizado.toFixed(0) + "%</b></h5>" +
        "   <h5><b>" + comentario + "</b></h5>" +
        "</div>"
    );
    parseToCurrency();
}