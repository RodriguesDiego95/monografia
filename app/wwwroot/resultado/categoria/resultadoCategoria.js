function resultadoCategoriaIndex() {
    renderHtml("#conteudo", "/Resultado/Categoria/index.html","configuraComponentes();");
}

function configuraComponentes() {
    $("#titulo-nav").text("Resultados por Categorias");
    $(".side-nav").sideNav("hide");
    configuraCalendario();
    configuraCollapsible();
    var dataAtual = currentDate()
    $(".datepicker").val(dataAtual);
    $(".datepicker").next("label").attr("class", "active");
    //loadChartResultadoCategoria(dataAtual,dataAtual);
}

function loadChartResultadoCategoria(dataInicial, dataFinal) {
    loadingOpen();

    dataInicial = dataInicial.toString();
    dataFinal = dataFinal.toString();

    $.ajax({
        type: "GET",
        url: getApi() + "/get/resultadoscategoria?datainicial=" + dateToJSON(dataInicial) + "&datafinal=" + dateToJSON(dataFinal),
        cache: true,
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
        success: function (retorno) {
            preparaChartResultadoCategoria(retorno.resultadoCategoria);
        },
        error: function (response) {
            alert("Erro ao recuperar resultados de categorias.");
        },
        complete: function () {
            loadingClose();
        }
    });
}

function preparaChartResultadoCategoria(dados) {
    $("#conteudo-resultadoCategoria").empty();
    for (var i = 0; i < dados.length; i++) {
        $("#conteudo-resultadoCategoria").append(
            "<div class='card col s12 m8 l6 offset-m2'>" +
                "<div class='col s6'>" +
                    "<h4>" + dados[i].usuario.nomeCompleto +"</h4>" +
                    "<div class='divider'></div>" +
                    "<div class='section'>" +
                        "<h5>Tempo</h5>" +
                        "<span>Produtividade: " + dados[i].textoProdutividade + "</span><br>" +
                        "<span>Investimentos: " + dados[i].textoInvestimento + "</span><br>" +
                        "<span>Perdas: " + dados[i].textoPerda + "</span><br>" +
                    "</div>" +
                    "<div class='divider'></div>" +
                    "<div class='section'>" +
                        "<h5>Custo</h5>" +
                        "Produtividade: <span class='currency'>" + dados[i].custoProdutividade.toFixed(2) + "</span><br>" +
                        "Investimentos: <span class='currency'>" + dados[i].custoInvestimento.toFixed(2) + "</span><br>" +
                        "Perdas: <span class='currency'>" + dados[i].custoPerda.toFixed(2) + "</span><br>" +
                    "</div>" +
                "</div>" +
                "<div class='col s6'>" +
                    "<canvas id='chartResultadoCategoria_" + i + "' width='400' height='600'></canvas>"+
                "</div>" +
            "</div>" 
        )
        //configuraMoney();
        parseToCurrency();
        buildChartResultadoCategoria(dados[i], i);
    }
    if (dados.length == 0) {
        $("#conteudo-resultadoCategoria").append(
            "<div class='card'>" +
            "   <div class='card-content'>" +
            "       <span class='card-title center'>Não possui dados no período!</span>" +
            "   </div>" +
            "</div>"
        );
    }
}

function buildChartResultadoCategoria(result, index) {
    var ctx = document.getElementById("chartResultadoCategoria_" + index);

    data = {
        datasets: [{
            backgroundColor: ['#0091ea', '#ffff00', '#d50000'],
            data: [result.percentualProdutividade.toFixed(1), result.percentualInvestimento.toFixed(1), result.percentualPerda.toFixed(1)]
        }],
        labels: [
            '(%) Produtividades',
            '(%) Investimentos',
            '(%) Perdas'
        ]
    }
    var chartResultado = new Chart(ctx, {
        type: 'pie',
        data: data
    });
}

function aplicarFiltroResultadoCategoria() {
    var dataInicial = $("#data_inicio").val();
    var dataFinal = $("#data_fim").val();
    loadChartResultadoCategoria(dataInicial, dataFinal)
}