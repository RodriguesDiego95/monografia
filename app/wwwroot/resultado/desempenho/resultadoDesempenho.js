function resultadoDesempenhoIndex() {
    renderHtml("#conteudo", "/Resultado/Desempenho/index.html", "configuraComponentesResultadoDesempenho();");
}

function configuraComponentesResultadoDesempenho() {
    $("#titulo-nav").text("Resultados de Desempenho");
    $(".side-nav").sideNav("hide");
    configuraCollapsible();
    obtemProjetosResultado();
}

function obtemProjetosResultado() {
    loadingOpen();
    $.ajax({
        type: "GET",
        url: getApi() + "/get/projeto",
        cache: false,
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
        success: function (retorno) {
            preencheSelectProjeto(retorno.projetos)
        },
        error: function (response) {
            alert(response.responseText);
        },
        complete: function () {
            loadingClose();
        }
    });
}

function preencheSelectProjeto(projetos) {
    for (var i = 0; i < projetos.length; i++) {
        $("#selectProjeto").append(
            "<option value='" + projetos[i].projetoId + "'>" + projetos[i].nome + "</option>"
        );
    }
    configuraSelect();
}

function aplicarFiltroResultadoDesempenho() {
    loadingOpen();

    $.ajax({
        type: "GET",
        url: getApi() + "/get/resultadosdesempenho?projetoid=" + $("#selectProjeto").val(),
        cache: false,
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
        success: function (retorno) {
            preparaChartResultadoDesempenho(retorno.resultadosDesempenho);
        },
        error: function (response) {
            alert(response.responseText);
        },
        complete: function () {
            loadingClose();
        }
    });
}

function preparaChartResultadoDesempenho(resultadosDesempenho) {
    $("#conteudo-resultadoDesempenho").empty();
    if (resultadosDesempenho.length == 0) {
        $("#conteudo-resultadoDesempenho").append(
            "<div class='card'>" +
            "   <div class='card-content'>" +
            "       <span class='card-title center'>Não há dados a ser exibido!</span>" +
            "   </div>" +
            "</div>"
        );
    } else {
        for (var i = 0; i < resultadosDesempenho.length; i++) {
            $("#conteudo-resultadoDesempenho").append(
                "<div class='card col s12 m12 l12'>" +
                "   <h5 class='center'>" + resultadosDesempenho[i].usuario.nomeCompleto + "</h5>" +
                "   <div class='col s12 m12 l5'>" +
                "       <canvas id='chartResultadoDesempenho_" + i + "' width='400' height='250'></canvas>" +
                "   </div>" +
                "   <div id='info-resultadoDesempenho_" + i + "' class='col s12 m12 l7'>" +
                "   </div>" +
                "</div>"
            );
            parseToCurrency();
            buildChartResultadoDesempenho(resultadosDesempenho[i], i);
            infoResultadoDesempenho(resultadosDesempenho[i], i);
        }
    }
}

function buildChartResultadoDesempenho(result, index) {
    var ctx = document.getElementById("chartResultadoDesempenho_" + index);

    var data = {
        datasets: [{
            label: ['Resultado em Horas'],
            data: [result.somaPrevisaoTempo.toFixed(2), result.tempoGasto.toFixed(2)],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)'
            ],
            borderWidth: 1
        }],
        labels: ['Tempo estipulado', 'Realizado']
    }
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

function infoResultadoDesempenho(result, index) {
    $("#info-resultadoDesempenho_" + index).append(
        "<div class='row'></div>" +
        "<div class='col s6'>" +
        "   <span><b>Valores estipulados:</b></span>" +
        "   <p><span>Tempo: " + result.somaPrevisaoTempo + ":00:00</span></p>" +
        "   <p>Custo: <span class='currency'> " + result.custoPrevisto.toFixed(2) + "</span></p>" +
        "</div>" +
        "<div class='col s6'>" +
        "   <span><b>Realizado:</b></span>" +
        "   <p><span>Tempo: " + result.textoTempoGasto + "</span></p>" +
        "   <p>Custo: <span class='currency'>" + result.custoRealizado.toFixed(2) + "</span></p>" +
        "</div>" +
        "<div class='divider'></div>" +
        "<div class='row'></div>" +
        "<div class='section'>" +
        "   <h5>Percentual de conclusão das tarefas: <b>" + result.percentualProjetoFinalizado.toFixed(0) + "%</b></h5>" +
        "</div>"
    );
    parseToCurrency();
}