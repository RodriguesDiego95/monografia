function resultadoApontamentoIndex() {
    renderHtml("#conteudo", "/Resultado/Apontamento/index.html", "configuraComponentesResultadoApontamento();");
}

function configuraComponentesResultadoApontamento() {
    $("#titulo-nav").text("Visualizar apontamentos");
    $(".side-nav").sideNav("hide");
    var dataAtual = currentDate()
    $(".datepicker").val(dataAtual);
    $(".datepicker").next("label").attr("class", "active");
    configuraCalendario();
    configuraCollapsible();
    atualizaSelectColaborador();
}

function atualizaSelectColaborador() {
    obtemColaboradores();
}

function obtemColaboradores() {
    loadingOpen();
    $.ajax({
        type: "GET",
        url: getApi() + "/getAll/usuario",
        cache: false,
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
        success: function (retorno) {
            preencheSelectColaborador(retorno.usuarios)
            configuraSelect();
        },
        error: function (response) {
            alert(response.responseJSON.response.message);
        },
        complete: function () {
            loadingClose();
        }
    });
}

function preencheSelectColaborador(usuarios) {
    for (var i = 0; i < usuarios.length; i++) {
        $("#selectColaborador").append(
            "<option value='" + usuarios[i].usuarioId + "'>" + usuarios[i].nomeCompleto + "</option>"
        );
    }
}

function aplicarFiltroResultadoApontamento() {
    var dataInicial = $("#data_inicio").val();
    var dataFinal = $("#data_fim").val();
    var usuarioId = $("#selectColaborador").val();
    loadResultadoApontamento(usuarioId, dataInicial, dataFinal);
}

function loadResultadoApontamento(usuarioId, dataInicial, dataFinal) {
    dataInicial = dataInicial.toString();
    dataFinal = dataFinal.toString();
    loadingOpen();
    $.ajax({
        type: "GET",
        url: getApi() + "/get/visualizarapontamento?usuarioid=" + usuarioId + "&" + "datainicial=" + dateToJSON(dataInicial) + "&" + "datafinal=" + dateToJSON(dataFinal),
        cache: true,
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
        success: function (retorno) {
            configuraCalendario();
            exibeDadosApontamento2(retorno.apontamentos);
        },
        error: function (response) {
            alert("Erro ao recuperar resultados de categorias.");
        },
        complete: function () {
            loadingClose();
        }
    });
    configuraCollapsible();
}

function exibeDadosApontamento2(apontamentos) {
    var html = "";
    if (apontamentos.length > 0) {
        $("#conteudo-apontamentos").empty();
        $("#conteudo-apontamentos").append(`
            <div class='card'>
                <div class='card-content'>
                    <div class="col s12 m10 l8 push-m1 push-l2">
                    <div class="center legendaApontamento col s4 card-panel light-blue lighten-4">Produtividades</div>
                    <div class="center legendaApontamento col s4 card-panel yellow lighten-3">Investimentos</div>
                    <div class="center legendaApontamento col s4 card-panel red lighten-3">Perdas</div>
                    </div>
                    <table id="tableApontamentos" class="highlight">
                        <thead>
                            <tr>
                                <th class="col s2 center">Data</th>
                                <th class="col s1 center">Início</th>
                                <th class="col s1 center">Término</th>
                                <th class="col s4">Descrição</th>
                                <th class="col s4">Tarefa</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>`
        );
        for (var i = 0; i < apontamentos.length; i++) {
            var tarefaDescricao = "";
            if (apontamentos[i].tarefaFinalizada) {
                tarefaDescricao = apontamentos[i].tarefa.descricao + " (Tarefa finalizada)";
            } else {
                tarefaDescricao = apontamentos[i].tarefa.descricao
            }
            var cor = "";
            if (getCategoriaType(apontamentos[i].categoria) == "Produtividade") {
                cor = "light-blue lighten-4"
            } else if (getCategoriaType(apontamentos[i].categoria) == "Investimento") {
                cor = "yellow lighten-3"
            } else if (getCategoriaType(apontamentos[i].categoria) == "Perda gerencial") {
                cor = "red lighten-3"
            }
            $("#tableApontamentos > tbody").append(
                "<tr class='" + cor + "'>" +
                "   <td class='col s2 center'>" + formatDate(apontamentos[i].data) + "</td>" +
                "   <td class='col s1 center'>" + apontamentos[i].horaInicial + "</td>" +
                "   <td class='col s1 center'>" + apontamentos[i].horaFinal + "</td>" +
                "   <td class='col s4'>" + apontamentos[i].descricao + "</td>" +
                "   <td class='col s4'>" + tarefaDescricao + "</td>" +
                "</tr>"
            );
        }
    }
    if (apontamentos.length == 0) {
        html += "<div class='card'>"
        html += "   <div class='card-content'>"
        html += "       <span class='card-title center'>Não possui dados no período!</span>"
        html += "   </div>"
        html += "</div>"
        $("#conteudo-apontamentos").empty();
        $("#conteudo-apontamentos").append(html)
    }
    configuraCollapsible();
}

function exibeDadosApontamento(apontamentos) {
    var html = ""
    if (apontamentos.length > 0) {
        html += '<ul class="collapsible" data-collapsible="expandable">'
        for (var i = 0; i < apontamentos.length; i++) {
            html += '<li>' +
                '   <div class="collapsible-header">' +
                '       <div class="col s2">' +
                '           <b>Data:</b>' + formatDate(apontamentos[i].data) +
                '       </div>' +
                '       <div class="col s2">' +
                '           <b>Tempo gasto:</b>' + apontamentos[i].tempoGasto +
                '       </div>' +
                '       <div class="col s6">' +
                '           <b>Categoria:</b>' + getCategoriaType(apontamentos[i].categoria) +
                '       </div>' +
                '   </div>' +
                '   <div class="collapsible-body">' +
                '       <div class="row">' +
                '           <span>' + apontamentos[i].descricao + '</span>' +
                '       </div>' +
                '   </div>' +
                '</li>'
        }
        html += '</ul>'
    }
    if (apontamentos.length == 0) {
        html += "<div class='card'>"
        html += "   <div class='card-content'>"
        html += "       <span class='card-title center'>Não possui dados no período!</span>"
        html += "   </div>"
        html += "</div>"
    }
    $("#conteudo-apontamentos").empty();
    $("#conteudo-apontamentos").append(html)
    configuraCollapsible();
}