function usuarioIndex() {
    renderHtml("#conteudo", "/usuario/index.html", "loadUsuarioIndex();");
}

function loadUsuarioIndex() {
    $("#titulo-nav").text("Membros");
    $(".side-nav").sideNav("hide");
    obtemUsuarios();
    configuraMoney();
    loadModalNovoUsuario();
}

function obtemUsuarios() {
    loadingOpen();
    $.ajax({
        type: "GET",
        url: getApi() + "/getAll/usuario",
        cache: false,
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
        success: function (retorno) {
            preencheTableUsuarios(retorno.usuarios)
        },
        error: function (response) {
            //alert(response.responseText);
        },
        complete: function () {
            loadingClose();
        }
    });
}

function preencheTableUsuarios(usuarios) {
    $("#tableUsuarios > tbody").empty();
    for (var i = 0; i < usuarios.length; i++) {
        $("#tableUsuarios > tbody").append(
            "<tr id='" + usuarios[i].usuarioId + "'>" +
            "   <td class='col s1'>" + usuarios[i].usuarioId + "</td>" +
            "   <td class='col s6'>" + usuarios[i].nomeCompleto + "</td>" +
            "   <td class='col s3 money'>" + usuarios[i].valorHora.toFixed(2) + "</td>" +
            "   <td hidden='hidden'>" + usuarios[i].nomeUsuario + "</td>" +
            "   <td hidden='hidden'>" + usuarios[i].senha + "</td>" +
            "   <td hidden='hidden'>" + usuarios[i].gerente + "</td>" +
            "   <td class='col s2 editaregistro'>" +
            "       <div class='col s3 tooltipped' data-position='top' data-delay='50' data-tooltip='Editar'>" +
            "           <i class='material-icons modal-trigger' data-toggle='modal' data-target='modalNovoUsuario' onclick='editUsuario($(this));'>edit</i>" +
            "       </div>" +
            "       <div class='col s3 tooltipped' data-position='top' data-delay='50' data-tooltip='Excluir'>" +
            "           <i class='material-icons' onclick='removeUsuario($(this));'>delete</i>" +
            "       </div>" +
            "   </td>" +
            "   " +
            "</tr>"
        );
    }
    parseToMask();
    configuraTooltip();
    configuraMoney();
}

function loadModalNovoUsuario() {
    $.ajax({
        url: "/Usuario/form.html",
        dataType: "html",
        success: function (html) {
            if ($("#modalNovoUsuario > div").length == 0) {
                $("#modalNovoUsuario").append(html);
            }
            $("#modalNovoUsuario").modal();
        },
        error: function (html) {
            //alert("Erro!");
        },
        complete: function () {
            configuraMoney();
        }
    });
}

function newUsuario() {
    $("#usuarioid").val("0");
    $("#nome_completo").val("");
    $("#valor_hora").val("");
    $("#nome_usuario").val("");
    $("#senha").val("");
    $("#gerente").prop('checked',false);
    iniciaComponentes();
}

function editUsuario(selecionado) {
    var row = selecionado.closest("tr");
    $("#usuarioid").val(row.find("td:eq(0)").text());
    $("#nome_completo").val(row.find("td:eq(1)").text());
    $("#valor_hora").val(row.find("td:eq(2)").text());
    $("#nome_usuario").val(row.find("td:eq(3)").text());
    $("#senha").val(row.find("td:eq(4)").text());
    var gerente = row.find("td:eq(5)").text();
    if (gerente == "true") {
        $("#gerente").prop('checked',true);
    } else {
        $("#gerente").prop('checked',false);
    }
    atualizaComponentes();
}

function cadastrarUsuario() {
    loadingOpen();
    $.ajax({
        type: "POST",
        url: getApi() + "/add/usuario",
        data: JSON.stringify(retornaDadosUsuario()),
        cache: false,
        contentType: "application/json;charset=utf-8",
        headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
        success: function (retorno) {
            if (getResponseType(retorno.response.type) == "success") {
                $("#modalNovoUsuario").modal('close');
                obtemUsuarios();
            } else {
                sinalizaCamposInvalidos(retorno.response.validationErrors);
            }
        },
        error: function (retorno) {
            alert(retorno.responseText);
        },
        complete: function () {
            loadingClose();
        }
    });
}

function retornaDadosUsuario() {
    var valorH = removeMascara($("#valor_hora").val());
    valorH = valorH.replace(",",".");
    var gerente;
    if (valorH == "") {
        valorH = 0;
    }
    if ($("#gerente").is(":checked")) {
        gerente = true;
    } else {
        gerente = false;
    }
    var dados = {
        UsuarioId: $("#usuarioid").val(),
        NomeCompleto: $("#nome_completo").val(),
        ValorHora: valorH,
        NomeUsuario: $("#nome_usuario").val(),
        Senha: $("#senha").val(),
        Gerente: gerente
    };
    return dados;
}

function removeUsuario(selecionado) {
    if (confirm("Deseja realmente excluir o usuário?")) {
        var row = selecionado.closest("tr");
        var usuarioId = row.find("td:eq(0)").text()
        $.ajax({
            type: "DELETE",
            url: getApi() + "/delete/usuario?usuarioid=" + usuarioId,
            cache: false,
            contentType: "application/json;charset=utf-8",
            headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
            success: function (retorno) {
                if (getResponseType(retorno.response.type) == "warning") {
                    alert(retorno.response.message);
                } else {
                    obtemUsuarios();
                }
            },
            error: function (retorno) {
                //alert(retorno.responseJSON.response.message);
            }
        });
    }
}