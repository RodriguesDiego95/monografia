function projetoIndex() {
    renderHtml("#conteudo", "/Projeto/index.html", "loadProjetoIndex();");
}

function loadProjetoIndex() {
    $("#titulo-nav").text("Projetos");
    $(".side-nav").sideNav("hide");
    obtemProjetos();
    loadModalNovoProjeto();
}

function obtemProjetos() {
    loadingOpen();
    $.ajax({
        type: "GET",
        url: getApi() + "/get/projeto",
        cache: false,
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
        success: function (retorno) {
            preencheTableProjetos(retorno.projetos)
        },
        error: function (response) {
            //alert(response.responseText);
        },
        complete: function () {
            loadingClose();
        }
    });
}

function preencheTableProjetos(projetos) {
    $("#tableProjetos > tbody").empty();
    for (var i = 0; i < projetos.length; i++) {
        $("#tableProjetos > tbody").append(
            "<tr id='" + projetos[i].projetoId + "'>" +
            "<td class='col s1'>" + projetos[i].projetoId + "</td>" +
            "<td class='col l3 m2 s7'>" + projetos[i].nome + "</td>" +
            "<td class='col l4 m5 s3'>" + projetos[i].descricao + "</td>" +
            "<td class='col l2 m2 s3'>" +
            "   <a id='botao-tarefas' class='waves-effect waves-light btn' onclick='paginaTarefas($(this));'>" +
            "   Tarefas</a>" +
            "</td>" +
            "<td class='col l2 m1 s3 editaregistro left'>" +
            // "   <div class='col s3'>" +
            // "       <i id='tarefasProjeto' class='material-icons' onclick='paginaTarefas($(this));'>content_paste</i>" +
            // "   </div>" +
            "   <div class='col s3 tooltipped' data-position='top' data-delay='50' data-tooltip='Editar'>" +
            "       <i id='editarProjeto' class='material-icons modal-trigger' data-toggle='modal' data-target='modalNovoProjeto' onclick='editProjeto($(this));'>edit</i>" +
            "   </div>" +
            "   <div class='col s3 tooltipped' data-position='top' data-delay='50' data-tooltip='Excluir'>" +
            "       <i id='excluirProjeto' class='material-icons' onclick='removeProjeto($(this));'>delete</i>" +
            "   </div>" +
            "</td>" +
            "</tr>"
        );
    }
    configuraTooltip();
}

function editProjeto(selecionado) {
    var row = selecionado.closest("tr");
    $("#projetoid").val(row.find("td:eq(0)").text());
    $("#nome").val(row.find("td:eq(1)").text());
    $("#descricao").val(row.find("td:eq(2)").text());
    atualizaComponentes();
}

function removeProjeto(selecionado) {
    if (confirm("Deseja realmente excluir o projeto?")) {
        var row = selecionado.closest("tr");
        var projetoId = row.find("td:eq(0)").text()
        $.ajax({
            type: "DELETE",
            url: getApi() + "/delete/projeto?projetoid=" + projetoId,
            cache: false,
            contentType: "application/json;charset=utf-8",
            headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
            success: function (retorno) {
                if (getResponseType(retorno.response.type) == "warning") {
                    alert(retorno.response.message);
                } else {
                    obtemProjetos();
                }
            },
            error: function (retorno) {
                //alert(retorno.responseJSON.response.message);
            }
        });
    }
}

function newProjeto() {
    $("#projetoid").val("0");
    $("#nome").val("");
    $("#descricao").val("");
    iniciaComponentes();
}

function paginaTarefas(selecionado) {
    var row = selecionado.closest("tr");
    var projetoId = row.find("td:eq(0)").text();
    tarefaIndex(projetoId);
}

function loadProjetoCreate() {
    renderHtml("#modalNovoProjeto", "/Projeto/form.html");
}

function loadModalNovoProjeto() {
    $.ajax({
        url: "/Projeto/form.html",
        dataType: "html",
        success: function (html) {
            if ($("#modalNovoProjeto > div").length == 0) {
                $("#modalNovoProjeto").append(html);
            }
            $("#modalNovoProjeto").modal();
        },
        error: function (html) {
            //alert("Erro!");
        },
        complete: function () {

        }
    });
}

function cadastrarProjeto() {
    loadingOpen();
    $.ajax({
        type: "POST",
        url: getApi() + "/add/projeto",
        data: JSON.stringify(retornaDadosProjeto()),
        cache: false,
        contentType: "application/json;charset=utf-8",
        headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
        success: function (retorno) {
            if (getResponseType(retorno.response.type) == "success") {
                $("#modalNovoProjeto").modal('close');
                obtemProjetos();
            } else {
                sinalizaCamposInvalidos(retorno.response.validationErrors);
            }
        },
        error: function (retorno) {
            alert(retorno.responseJSON.response.message);
        },
        complete: function () {
            loadingClose();
        }
    });
}

function retornaDadosProjeto() {
    var dados = {
        ProjetoId: $("#projetoid").val(),
        Nome: $("#nome").val(),
        Descricao: $("#descricao").val()
    };
    return dados;
}
