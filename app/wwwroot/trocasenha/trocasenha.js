function renderScreenTrocaSenha() {
    $(".side-nav").sideNav("hide");
    renderHtml("#app", "Trocasenha/index.html");
}

function confirmaTrocaSenha() {
    if ($("#senha_nova").val() == $("#senha_confirma").val()) {
        if (confirm("Deseja realmente trocar a senha?")) {
            realizaTrocaSenha();
        }
    } else {
        alert("As senhas não conferem!");
    }
}

function realizaTrocaSenha() {
    var dados = {
        senhaAntiga: $("#senha_antiga").val(),
        senhaNova: $("#senha_nova").val()
    };
    $.ajax({
        type: "PUT",
        url: getApi() + "/put/trocasenha?senhaantiga=" + dados.senhaAntiga + "&senhanova=" + dados.senhaNova,
        cache: false,
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
        success: function (retorno) {
            if (getResponseType(retorno.response.type) == "warning") {
                alert(retorno.response.message);
            } else {
                alert("Senha salva com sucesso!");
                logout();
            }
        },
        error: function () {
            alert("Não foi realizado a troca de senha! Confirme os dados inseridos.");
        }
    });
}