// Server
function getApi() {
    var servidor = "http://localhost:3000/api";
    //var servidor = "http://192.168.0.139:8585/api";
    return servidor;
}

function renderHtml(parent, content, callback) {
    $(parent).load(content, function () {
        eval(callback);
    });
}

function getResponseType(type) {
    if (type == 1) {
        return "success";
    }
    else if (type == 2) {
        return "warning";
    }
    else {
        return "error";
    }
}

function getCategoriaType(type) {
    if (type == 1) {
        return "Produtividade";
    }
    else if (type == 2) {
        return "Investimento";
    }
    else {
        return "Perda gerencial";
    }
}

function sinalizaCamposInvalidos(inputs) {
    for (var i = 0; i < inputs.length; i++) {
        var input = $("#" + inputs[i].field);
        input.next("label").attr("data-error", inputs[i].message);
        input.next("label").attr("class", "active");
        input.removeClass("valid");
        input.addClass("invalid");
    }
}

function iniciaComponentes() {
    $(".validate").removeClass("valid");
    $(".validate").removeClass("invalid");
    $(".validate").next("label").attr("class", "inactive");
}

function atualizaComponentes() {
    $(".validate").removeClass("valid");
    $(".validate").removeClass("invalid");
    $(".validate").next("label").attr("class", "active");
}


function dateToJSON(date) {
    // Converte datas do formato DD/MM/AAAA para formatos de data para o JSON: AAAA-MM-DD
    if (date.length < 10) {
        return "0001-01-01"
    }
    var ret = date.substring(6, 10) + "-";
    ret += date.substring(3, 5) + "-";
    ret += date.substring(0, 2)
    return ret
}

function formatDate(date) {
    // Converte datas do formato do JSON AAAA-MM-DD para o britânico: DD/MM/AAAA
    if (date.length < 10) {
        return ""
    }
    var ret = date.substring(8, 10) + "/";
    ret += date.substring(5, 7) + "/";
    ret += date.substring(0, 4);
    return ret
}

function currentDate() {
    // Retorna data atual em string com o formato DD/MM/AAAA
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10) {
        dd = '0'+dd
    } 
    if(mm<10) {
        mm = '0'+mm
    }
    today = dd + '/' + mm + '/' + yyyy;
    return today;
}

function loadingOpen() {
    
    if (!$("#holdon-overlay").length) {
        var options = {
            theme: "sk-bounce"
        };
        HoldOn.open(options);
    }
    
};

function loadingClose() {
    HoldOn.close();
};

function configuraCalendario() {
    $('.datepicker').pickadate({
        selectMonths: true,
        selectYears: 15,
        format: 'dd/mm/yyyy',
        closeOnSelect: true,
        //container: conteudo,
        container:"",
        labelMonthNext: 'Próximo Mês',
        labelMonthPrev: 'Mês Anterior',
        labelMonthSelect: 'Selecionar mês',
        labelYearSelect: 'Selecionar ano',
        monthsFull: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho",
            "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
        monthsShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
        weekdaysFull: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
        weekdaysShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
        weekdaysLetter: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
        today: 'Hoje',
        clear: 'Limpar',
        close: false
    });
}

function configuraRelogio() {
    $('.timepicker').pickatime({
        default: 'now', // Set default time: 'now', '1:30AM', '16:30'
        fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
        twelvehour: false, // Use AM/PM or 24-hour format
        donetext: 'Ok', // text for done-button
        cleartext: 'Limpar', // text for clear-button
        canceltext: 'Cancelar', // Text for cancel-button,
        container: 'body', // ex. 'body' will append picker to body
        autoclose: true, // automatic close timepicker
        ampmclickable: false, // make AM PM clickable
        close: false
    });
}

function configuraSelect() {
    $('select').material_select();
}

function configuraHora() {
    $('.time').mask('00:00');
}

function configuraMoney() {
    $('.money').mask('000.000.000.000,00', {reverse: true});
}

function configuraTooltip() {
    $('.tooltipped').tooltip({ delay: 50 });
}

function configuraCollapsible() {
    $('.collapsible').collapsible();
}

function parseToMask() {
    $(".maskCPF").mask("000.000.000-00", { placeholder: "___.___.___-__" });
    $(".maskCNPJ").mask("00.000.000/0000-00", { placeholder: "__.___.___/____-__" });
    $(".maskPhone").mask("(000) 000000000", { placeholder: "(___) _________" });
    $(".maskCEP").mask("00000-000", { placeholder: "_____-___" });

    parseToDecimal();
    parseToCurrency();
    parseToPercent();
}

function parseToDecimal() {
    $(".decimal").priceFormat({
        prefix: "",
        centsSeparator: ",",
        thousandsSeparator: "."
    });
}

function parseToCurrency() {
    $(".currency").priceFormat({
        prefix: "R$ ",
        centsSeparator: ",",
        thousandsSeparator: ".",
        allowNegative: true
    });
}

function parseToPercent() {
    $(".percent").priceFormat({
        prefix: "",
        suffix: "%",
        centsSeparator: ",",
        thousandsSeparator: ".",
        allowNegative: true
    });
}

function removeMascara(texto) {
    texto = texto.replace(/\(/g, "");
    texto = texto.replace(/\)/g, "");
    texto = texto.replace(/\//g, "");
    texto = texto.replace(/\./g, "");
    texto = texto.replace(/\-/g, "");
    texto = texto.replace(" ", "");
    return texto;
}

function decimalToString(value) {
    value = value.replace(/\./g, '');
    value = value.replace('R$ ', '');
    value = value.replace('%', '');
    value = value.replace(',', '.');
    return value;
}

function RemoveInvalid_datepicker() {
    $(".datepicker").removeClass("invalid");
}

function formatNumber(number, decimalsLength, decimalSeparator, thousandSeparator) {
    var n = number,
        decimalsLength = isNaN(decimalsLength = Math.abs(decimalsLength)) ? 2 : decimalsLength,
        decimalSeparator = decimalSeparator == undefined ? "," : decimalSeparator,
        thousandSeparator = thousandSeparator == undefined ? "." : thousandSeparator,
        sign = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(decimalsLength)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;

    return sign +
        (j ? i.substr(0, j) + thousandSeparator : "") +
        i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousandSeparator) +
        (decimalsLength ? decimalSeparator + Math.abs(n - i).toFixed(decimalsLength).slice(2) : "");
}