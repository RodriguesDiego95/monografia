function apontamentoIndex() {
    renderHtml("#conteudo", "/apontamento/index.html", "loadApontamentoIndex();");
}

function loadApontamentoIndex() {
    $("#titulo-nav").text("Apontamentos");
    $(".side-nav").sideNav("hide");
    obtemApontamentos();
    loadModalNovoApontamento();
}

function obtemApontamentos() {
    loadingOpen();
    $.ajax({
        type: "GET",
        url: getApi() + "/get/apontamento",
        cache: false,
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
        success: function (retorno) {
            preencheTableApontamentos(retorno.apontamentos)
        },
        error: function (response) {
            //alert(response.responseText);
        },
        complete: function () {
            loadingClose();
        }
    });
}

function preencheTableApontamentos(apontamentos) {
    $("#tableApontamentos > tbody").empty();
    for (var i = 0; i < apontamentos.length; i++) {
        $("#tableApontamentos > tbody").append(
            "<tr id='" + apontamentos[i].tarefaId + "'>" +
            "   <td class='col s1'>" + apontamentos[i].apontamentoId + "</td>" +
            "   <td class='col s5'>" + apontamentos[i].descricao + "</td>" +
            "   <td class='col s2 center'>" + formatDate(apontamentos[i].data) + "</td>" +
            "   <td class='col s1 center'>" + apontamentos[i].horaInicial + "</td>" +
            "   <td class='col s1 center'>" + apontamentos[i].horaFinal + "</td>" +
            "   <td hidden='hidden'>" + apontamentos[i].categoria + "</td>" +
            "   <td hidden='hidden'>" + apontamentos[i].tarefa.tarefaId + "</td>" +
            "   <td hidden='hidden'>" + apontamentos[i].projeto.projetoId + "</td>" +
            "   <td hidden='hidden'>" + apontamentos[i].usuario.usuarioId + "</td>" +
            "   <td hidden='hidden'>" + apontamentos[i].tarefaFinalizada + "</td>" +
            "   <td class='col s2 editaregistro'>" +
            "       <div class='col s3 tooltipped' data-position='top' data-delay='50' data-tooltip='Editar'>" +
            "           <i class='material-icons modal-trigger' data-toggle='modal' data-target='modalNovoApontamento' onclick='editApontamento($(this));'>edit</i>" +
            "       </div>" +
            "       <div class='col s3 tooltipped' data-position='top' data-delay='50' data-tooltip='Excluir'>" +
            "           <i class='material-icons' onclick='removeApontamento($(this));'>delete</i>" +
            "       </div>" +
            "   </td>" +
            "   " +
            "</tr>"
        );
    }
    //parseToMask();
    configuraTooltip();
}


function loadModalNovoApontamento() {
    $.ajax({
        url: "/Apontamento/form.html",
        dataType: "html",
        success: function (html) {
            if ($("#modalNovoApontamento > div").length == 0) {
                $("#modalNovoApontamento").append(html);
            }
            $("#modalNovoApontamento").modal();
            configuraCalendario();
            configuraRelogio();
            atualizaSelectTarefas();
        },
        error: function (html) {
            alert("Erro ao construir modal");
        },
        complete: function () {

        }
    });
}

function newApontamento() {
    $("#apontamentoid").val("0");
    $("#data").val(currentDate());
    $("#hora_inicial").val("");
    $("#hora_final").val("");
    $("#descricao").val("");
    $("#selectCategoria").val("1");
    $("#selectTarefa").val("0");
    $("#tarefa_finalizada").prop('checked',false);
    $("#tarefa_finalizada").attr("disabled", true);
    configuraSelect();
    configuraHora();
    iniciaComponentes();
    $(".datepicker").next("label").attr("class", "active");
}

function editApontamento(selecionado) {
    var row = selecionado.closest("tr");
    $("#apontamentoid").val(row.find("td:eq(0)").text());
    $("#descricao").val(row.find("td:eq(1)").text());
    $("#data").val(row.find("td:eq(2)").text());
    $("#hora_inicial").val(row.find("td:eq(3)").text().substring(0, 5));
    $("#hora_final").val(row.find("td:eq(4)").text().substring(0, 5));
    $("#selectCategoria").val(row.find("td:eq(5)").text());
    $("#selectTarefa").val(row.find("td:eq(6)").text());
    $("#tarefa_finalizada").removeAttr("disabled");
    if ($("#selectTarefa").val() > 0) {
        var tarefaFinalizada = row.find("td:eq(9)").text()
        if (tarefaFinalizada == "true") {
            $("#tarefa_finalizada").prop('checked',true);
        } else {
            $("#tarefa_finalizada").prop('checked',false);
        }
    } else {
        $("#tarefa_finalizada").prop('checked',false);
        $("#tarefa_finalizada").attr("disabled", true);
    }
    configuraSelect();
    configuraHora();
    atualizaComponentes();
}

function removeApontamento(selecionado) {
    var row = selecionado.closest("tr");
    if (confirm("Deseja realmente excluir o apontamento?")) {
        var row = selecionado.closest("tr");
        var apontamentoId = row.find("td:eq(0)").text()
        $.ajax({
            type: "DELETE",
            url: getApi() + "/delete/apontamento?apontamentoId=" + apontamentoId,
            cache: false,
            contentType: "application/json;charset=utf-8",
            headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
            success: function (retorno) {
                if (getResponseType(retorno.response.type) == "warning") {
                    alert(retorno.response.message);
                } else {
                    obtemApontamentos();
                }
            },
            error: function (retorno) {
                //alert(retorno.responseJSON.response.message);
            }
        });
    }
}

function cadastrarApontamento() {
    loadingOpen();
    $.ajax({
        type: "POST",
        url: getApi() + "/add/apontamento",
        data: JSON.stringify(retornaDadosApontamento()),
        cache: false,
        contentType: "application/json;charset=utf-8",
        headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
        success: function (retorno) {
            if (getResponseType(retorno.response.type) == "success") {
                $("#modalNovoApontamento").modal('close');
                obtemApontamentos();
            } else {
                sinalizaCamposInvalidos(retorno.response.validationErrors);
            }
        },
        error: function (retorno) {
            alert("Erro ao registrar o apontamento! Confira os dados inseridos.");
        },
        complete: function () {
            loadingClose();
        }
    });
}

function retornaDadosApontamento() {
    var data = dateToJSON($("#data").val())
    var horaI = $("#hora_inicial").val()
    var horaF = $("#hora_final").val()
    if (horaI == "") {
        horaI = "00:00"
    }
    if (horaF == "") {
        horaF = "00:00"
    }
    var tarefaFinalizada;
    if ($("#tarefa_finalizada").is(":checked")) {
        tarefaFinalizada = true;
    } else {
        tarefaFinalizada = false;
    }
    var dados = {
        ApontamentoId: $("#apontamentoid").val(),
        Descricao: $("#descricao").val(),
        Data: data,
        HoraInicial: horaI,
        HoraFinal: horaF,
        Categoria: $("#selectCategoria").val(),
        Usuario: {
            UsuarioId: 1, //$("#usuarioAtual").val()
        },
        Tarefa: {
            TarefaId: $("#selectTarefa").val()
        },
        TarefaFinalizada: tarefaFinalizada
    };
    return dados;
}

function atualizaSelectTarefas() {
    obtemTarefasDoUsuario();
}

function obtemTarefasDoUsuario() {
    
    loadingOpen();
    $.ajax({
        type: "GET",
        url: getApi() + "/get/tarefasdousuario",
        cache: false,
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
        success: function (retorno) {
            preencheSelectTarefas(retorno.tarefas)
        },
        error: function (response) {
            //alert(response.responseJSON.response.message);
        },
        complete: function () {
            loadingClose();
        }
    });
}

function preencheSelectTarefas(tarefas) {
    for (var i = 0; i < tarefas.length; i++) {
        $("#selectTarefa").append(
            "<option value='"+ tarefas[i].tarefaId + "'>" + tarefas[i].descricao + "</option>"
        );
    }
}

function atualizaCheckFinalizada() {
    var tarefa = $("#selectTarefa").val();
    if (tarefa > 0) {
        $("#tarefa_finalizada").removeAttr("disabled");
    } else {
        $("#tarefa_finalizada").prop('checked',false);
        $("#tarefa_finalizada").attr("disabled", true);
    }
}