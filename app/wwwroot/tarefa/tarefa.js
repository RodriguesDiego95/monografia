function tarefaIndex(projetoId) {
    renderHtml("#conteudo", "/Tarefa/index.html", "loadTarefaIndex('" + projetoId + "');");
}

function loadTarefaIndex(projetoId) {
    $("#titulo-nav").text("Tarefas");
    $(".side-nav").sideNav("hide");
    obtemTarefas(projetoId);
    loadModalNovaTarefa();
}

function obtemTarefas(projetoId) {
    $("#projeto_codigo").val(projetoId);
    loadingOpen();
    $.ajax({
        type: "GET",
        url: getApi() + "/get/tarefa?projetoid=" + projetoId,
        cache: false,
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
        success: function (retorno) {
            preencheTableTarefas(retorno.tarefas)
        },
        error: function (response) {
            //alert(response.responseText);
        },
        complete: function () {
            loadingClose();
        }
    });
}

function preencheTableTarefas(tarefas) {
    $("#tableTarefas > tbody").empty();
    for (var i = 0; i < tarefas.length; i++) {
        $("#tableTarefas > tbody").append(
            "<tr id='" + tarefas[i].tarefaId + "'>" +
            "   <td class='col s1'>" + tarefas[i].tarefaId + "</td>" +
            "   <td class='col s4'>" + tarefas[i].descricao + "</td>" +
            "   <td class='col s2 center'>" + formatDate(tarefas[i].previsaoInicio) + "</td>" +
            "   <td class='col s2 center'>" + formatDate(tarefas[i].previsaoFim) + "</td>" +
            "   <td class='col s1 center money'>" + tarefas[i].valorEstimado.toFixed(2) + "</td>" +
            "   <td hidden='hidden'>" + tarefas[i].previsaoTempoGasto + "</td>" +
            "   <td hidden='hidden'>" + tarefas[i].usuario.usuarioId + "</td>" +
            "   <td class='col s2 editaregistro'>" +
            "       <div class='col s3 tooltipped' data-position='top' data-delay='50' data-tooltip='Editar'>" +
            "           <i class='material-icons modal-trigger' data-toggle='modal' data-target='modalNovaTarefa' onclick='editTarefa($(this));'>edit</i>" +
            "       </div>" +
            "       <div class='col s3 tooltipped' data-position='top' data-delay='50' data-tooltip='Excluir'>" +
            "           <i class='material-icons' onclick='removeTarefa($(this));'>delete</i>" +
            "       </div>" +
            "   </td>" +
            "   " +
            "</tr>"
        );
    }
    //parseToMask();
    configuraTooltip();
    configuraMoney();
}

function loadModalNovaTarefa() {
    $.ajax({
        url: "/Tarefa/form.html",
        dataType: "html",
        success: function (html) {
           
            if ($("#modalNovaTarefa > div").length == 0) {
                $("#modalNovaTarefa").append(html);
            }
            $("#modalNovaTarefa").modal();
            atualizaSelectResponsavel();
        },
        error: function (html) {
            //alert("Erro!");
        },
        complete: function () {

        }
    });
}

function newTarefa() {
    $("#tarefaid").val("0");
    $("#previsao_inicio").val("");
    $("#previsao_fim").val("");
    $("#previsao_tempo_gasto").val("");
    $("#descricao").val("");
    $("#selectResponsavel").val("0");
    configuraSelect();
    iniciaComponentes();
    configuraMoney();
}

function editTarefa(selecionado) {
    var row = selecionado.closest("tr");
    $("#tarefaid").val(row.find("td:eq(0)").text());
    $("#descricao").val(row.find("td:eq(1)").text());
    $("#previsao_inicio").val(row.find("td:eq(2)").text());
    $("#previsao_fim").val(row.find("td:eq(3)").text());
    $("#previsao_tempo_gasto").val(row.find("td:eq(5)").text());
    $("#selectResponsavel").val(row.find("td:eq(6)").text());
    configuraSelect();
    atualizaComponentes();
}

function removeTarefa(selecionado) {
    if (confirm("Deseja realmente excluir a terefa?")) {
        var row = selecionado.closest("tr");
        var tarefaId = row.find("td:eq(0)").text()
        $.ajax({
            type: "DELETE",
            url: getApi() + "/delete/tarefa?tarefaId=" + tarefaId,
            cache: false,
            contentType: "application/json;charset=utf-8",
            headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
            success: function (retorno) {
                if (getResponseType(retorno.response.type) == "warning") {
                    alert(retorno.response.message);
                } else {
                    obtemTarefas($("#projeto_codigo").val());
                }
            },
            error: function (retorno) {
                //alert(retorno.responseJSON.response.message);
            }
        });
    }
}

function cadastrarTarefa() {
    loadingOpen();
    $.ajax({
        type: "POST",
        url: getApi() + "/add/tarefa",
        data: JSON.stringify(retornaDadosTarefa()),
        cache: false,
        contentType: "application/json;charset=utf-8",
        headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
        success: function (retorno) {
            if (getResponseType(retorno.response.type) == "success") {
                $("#modalNovaTarefa").modal('close');
                obtemTarefas($("#projeto_codigo").val());
            } else {
                sinalizaCamposInvalidos(retorno.response.validationErrors);
            }
        },
        error: function (retorno) {
            alert(retorno.responseJSON.response.message);
        },
        complete: function () {
            loadingClose();
        }
    });
}

function retornaDadosTarefa() {
    var inicio = dateToJSON($("#previsao_inicio").val())
    var termino = dateToJSON($("#previsao_fim").val())
    var horas = $("#previsao_tempo_gasto").val();
    if (horas == "") {
        horas = 0;
    }
    var dados = {
        TarefaId: $("#tarefaid").val(),
        Projeto: {
            ProjetoId: $("#projeto_codigo").val()
        },
        Descricao: $("#descricao").val(),
        PrevisaoInicio: inicio,
        PrevisaoFim: termino,
        PrevisaoTempoGasto: horas,
        Usuario: {
            UsuarioId: $("#selectResponsavel").val()
        }
    };
    return dados;
}

function atualizaSelectResponsavel() {
    obtemDesenvolvedores();
}

function obtemDesenvolvedores() {
    
    loadingOpen();
    $.ajax({
        type: "GET",
        url: getApi() + "/getAll/usuario",
        cache: false,
        dataType: "json",
        contentType: "application/json;charset=utf-8",
        headers: { "Authorization": "Bearer " + localStorage.getItem("jtoken") },
        success: function (retorno) {
            preencheSelectResponsavel(retorno.usuarios)
        },
        error: function (response) {
            //alert(response.responseJSON.response.message);
        },
        complete: function () {
            loadingClose();
        }
    });
}

function preencheSelectResponsavel(usuarios) {
    for (var i = 0; i < usuarios.length; i++) {
        $("#selectResponsavel").append(
            "<option value='"+ usuarios[i].usuarioId + "'>" + usuarios[i].nomeCompleto + "</option>"
        );
    }
}