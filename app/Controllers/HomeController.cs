using Microsoft.AspNetCore.Mvc;

namespace SommusFarma.Tools.App.Controllers {
    public class HomeController : Controller {
        public IActionResult Index () => View ();
    }
}